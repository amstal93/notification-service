package io.sirius.notification.util

data class DummyPayload(
    val firstName: String = "john",
    val lastName: String = "doe",
    val email: Email = Email()
) {
    data class Email(val value: String = "john@doe.com")
}