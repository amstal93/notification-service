package io.sirius.notification.controller

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.AbstractTest
import io.sirius.notification.model.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.Instant
import java.util.concurrent.TimeUnit

class TrackingControllerTest : AbstractTest() {

    private lateinit var expectedEvent: TrackingEvent
    private lateinit var timestamp: Instant
    private lateinit var userAgent: String
    private lateinit var objectMapper: ObjectMapper

    @BeforeEach
    fun before() {
        objectMapper = Jackson2ObjectMapperBuilder().build()
        timestamp = Instant.ofEpochSecond(1433333949)
        userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 " +
                "(KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"

        expectedEvent = TrackingEvent(
            notification = NotificationCallback.NotificationCallbackInfo(
                id = "123",
                type = NotificationInfo.Type.EMAIL
            ),
            provider = NotificationCallback.ProviderCallbackInfo(
                id = "test",
                type = "smtp"
            ),
            metadata = mapOf(
                "key1" to "value1",
                "key2" to "value2"
            ),
            eventType = TrackingEvent.Type.SENT,
            timestamp = timestamp,
            ipAddress = "192.168.0.1",
            userAgent = userAgent,
            email = "test@sirius.com",
            clickedUrl = "http://test.com"
        )
    }

    @Test
    fun testMandrillEvent() {
        val events = MandrillEvent(
            ts = timestamp,
            event = MandrillEvent.Type.send,
            ip = "192.168.0.1",
            user_agent = userAgent,
            url = "http://test.com",
            msg = MandrillEvent.Message(
                state = "test",
                email = "test@sirius.com",
                metadata = ProviderMetadata("123", "test", "smtp").apply {
                    metadata = mapOf(
                        "key1" to "value1",
                        "key2" to "value2"
                    )
                }
            )
        )

        mockMvc.perform(
            MockMvcRequestBuilders.post("/tracking/email/mandrill")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("mandrill_events", payload(listOf(events)))
        ).andExpect(MockMvcResultMatchers.status().isOk)

        assertThat(trackingQueue.pollLast(5, TimeUnit.SECONDS)!!).isEqualTo(expectedEvent)
    }

    @Test
    fun testMailjetEvent() {
        val event = MailjetEvent(
            event = MailjetEvent.Type.sent,
            time = timestamp,
            email = "test@sirius.com",
            ip = "192.168.0.1",
            Payload = objectMapper.writeValueAsString(ProviderMetadata("123", "test", "smtp").apply {
                metadata = mapOf(
                    "key1" to "value1",
                    "key2" to "value2"
                )
            }),
            agent = userAgent,
            url = "http://test.com"
        )

        mockMvc.perform(
            MockMvcRequestBuilders.post("/tracking/email/mailjet")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(listOf(event)))
        ).andExpect(MockMvcResultMatchers.status().isOk)

        assertThat(trackingQueue.pollLast(5, TimeUnit.SECONDS)!!).isEqualTo(expectedEvent)
    }

    @Test
    fun testSendGridEvent() {
        val events = SendGridEvent(
            email = "test@sirius.com",
            timestamp = timestamp,
            event = SendGridEvent.Type.delivered,
            ip = "192.168.0.1",
            useragent = userAgent,
            url = "http://test.com",
            metadata = mapOf(
                "providerId" to "test",
                "providerType" to "smtp",
                "notificationId" to "123",
                "metadata_key1" to "value1",
                "metadata_key2" to "value2"
            ),
            category = emptyList()
        )

        mockMvc.perform(
            MockMvcRequestBuilders.post("/tracking/email/sendgrid")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(listOf(events)))
        ).andExpect(MockMvcResultMatchers.status().isOk)

        assertThat(trackingQueue.pollLast(5, TimeUnit.SECONDS)!!).isEqualTo(expectedEvent)
    }
}