package io.sirius.notification.controller

import io.sirius.notification.AbstractTest
import io.sirius.notification.model.WebSocketInfo
import io.sirius.notification.util.DummyPayload
import io.sirius.notification.util.StompSessionHandlerImpl
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.messaging.converter.MappingJackson2MessageConverter
import org.springframework.messaging.converter.StringMessageConverter
import org.springframework.messaging.simp.stomp.StompSession
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.socket.client.WebSocketClient
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import org.springframework.web.socket.messaging.WebSocketStompClient
import org.springframework.web.socket.sockjs.client.SockJsClient
import org.springframework.web.socket.sockjs.client.WebSocketTransport
import java.util.concurrent.TimeUnit


class WebSocketControllerTest : AbstractTest() {

    private lateinit var client: WebSocketClient
    private lateinit var stompClient: WebSocketStompClient
    private lateinit var session: StompSession
    private lateinit var subscription: StompSession.Subscription

    @BeforeEach
    fun before() {
        client = StandardWebSocketClient()
        stompClient = WebSocketStompClient(client)

    }

    @AfterEach
    fun after() {
        stompClient.stop()
        subscription.unsubscribe()
        session.disconnect()
    }

    @Nested
    inner class SockJS {

        private lateinit var handler: StompSessionHandlerImpl<String>

        @BeforeEach
        fun before() {
            stompClient = WebSocketStompClient(SockJsClient(listOf(WebSocketTransport(client))))

            handler = StompSessionHandlerImpl(String::class.java)

            stompClient.messageConverter = StringMessageConverter()
            session = stompClient.connect("ws://localhost:$serverPort/ws", handler).get(2, TimeUnit.SECONDS)
            subscription = session.subscribe("/topic/room", handler)
        }

        @Test
        fun testTopic() {
            val webSocketInfo = WebSocketInfo(
                topic = "room",
                body = "Hello dude é@ç!ù"
            )

            // Send webhook
            mockMvc.perform(
                post("/api/websocket")
                    .with(httpBasic("sirius", "sirius"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(payload(webSocketInfo))
            ).andExpect(MockMvcResultMatchers.status().isOk)

            assertThat(handler.waitForMessage()).isEqualTo("Hello dude é@ç!ù")
        }

    }

    @Nested
    inner class StringMessage {

        private lateinit var handler: StompSessionHandlerImpl<String>

        @BeforeEach
        fun before() {
            handler = StompSessionHandlerImpl(String::class.java)

            stompClient.messageConverter = StringMessageConverter()
            session = stompClient.connect("ws://localhost:$serverPort/ws", handler).get(2, TimeUnit.SECONDS)
            subscription = session.subscribe("/topic/room", handler)
        }

        @Test
        fun testTopic() {
            val webSocketInfo = WebSocketInfo(
                topic = "room",
                body = "Hello dude é@ç!ù"
            )

            // Send webhook
            mockMvc.perform(
                post("/api/websocket")
                    .with(httpBasic("sirius","sirius"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(payload(webSocketInfo))
            ).andExpect(MockMvcResultMatchers.status().isOk)

            assertThat(handler.waitForMessage()).isEqualTo("Hello dude é@ç!ù")
        }
    }

    @Nested
    inner class JsonMessage {

        private lateinit var handler: StompSessionHandlerImpl<DummyPayload>

        @BeforeEach
        fun before() {
            handler = StompSessionHandlerImpl(DummyPayload::class.java)

            stompClient.messageConverter = MappingJackson2MessageConverter()
            session = stompClient.connect("ws://localhost:$serverPort/ws", handler).get(2, TimeUnit.SECONDS)
            subscription = session.subscribe("/topic/room", handler)
        }

        @Test
        fun testTopic() {
            val webSocketInfo = WebSocketInfo(
                topic = "room",
                body = DummyPayload()
            )

            // Send webhook
            mockMvc.perform(
                post("/api/websocket")
                    .with(httpBasic("sirius","sirius"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(payload(webSocketInfo))
            ).andExpect(MockMvcResultMatchers.status().isOk)

            assertThat(handler.waitForMessage()).isEqualTo(DummyPayload())
        }

    }
}