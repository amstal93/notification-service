package io.sirius.notification.controller

import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import io.sirius.notification.AbstractTest
import io.sirius.notification.model.*
import io.sirius.notification.util.MimeMessageExt.getAttachments
import io.sirius.notification.util.MimeMessageExt.getFromAddress
import io.sirius.notification.util.MimeMessageExt.getPlaintText
import io.sirius.notification.util.MimeMessageExt.getTo
import io.sirius.notification.util.toByteArray
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.concurrent.TimeUnit

class EmailingControllerTest : AbstractTest() {

    private lateinit var emailInfo: EmailInfo
    private lateinit var smtpServer1: GreenMail
    private lateinit var smtpServer2: GreenMail

    @BeforeEach
    fun before() {
        callbackDeque.clear()

        smtpServer1 = GreenMail(ServerSetup(25000, null, ServerSetup.PROTOCOL_SMTP))
        smtpServer1.start()

        smtpServer2 = GreenMail(ServerSetup(26000, null, ServerSetup.PROTOCOL_SMTP))
        smtpServer2.start()

        emailInfo = EmailInfo(
            id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
            from = EmailInfo.EmailAddress("from@test.com", "John"),
            to = listOf(EmailInfo.EmailAddress("to@test.com")),
            subject = "My subject",
            plainText = "My body"
        )
    }

    @AfterEach
    fun after() {
        smtpServer1.stop()
        smtpServer2.stop()
    }

    @Test
    fun testMissingAuthentication() {
        mockMvc.perform(
            post("/api/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isUnauthorized)
    }

    @Test
    fun testInvalidAuthentication() {
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("wrong","credentials"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isUnauthorized)
    }

    @Test
    fun testSendEmail() {
        // Send email
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("sirius","sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isOk)

        assertThat(smtpServer1.waitForIncomingEmail(3_000, 1)).isTrue
        assertThat(smtpServer1.receivedMessages)
            .hasSize(1)
            .satisfies {
                val msg = it[0]
                assertThat(msg.getFromAddress()).isEqualTo(EmailInfo.EmailAddress("from@test.com", "John"))
                assertThat(msg.getTo()).isEqualTo(listOf(EmailInfo.EmailAddress("to@test.com")))
                assertThat(msg.subject).isEqualTo("My subject")
                assertThat(msg.getPlaintText()).isEqualTo("My body")
            }
        assertThat(smtpServer2.receivedMessages).isEmpty()

        val callback: NotificationCallback = callbackDeque.pollLast(5, TimeUnit.SECONDS)!!
        assertThat(callback).isEqualTo(
            NotificationSuccessCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
                    type = NotificationInfo.Type.EMAIL
                ),
                counter = 1,
                tryIndex = 0,
                provider = NotificationCallback.ProviderCallbackInfo(
                    id = "smtp1",
                    type = "smtp"
                )
            )
        )
        assertThat(callbackErrorDeque).isEmpty()
    }

    @Test
    fun testSendEmailWithSpecificProvider() {
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("sirius","sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo.copy(providers = setOf("smtp2")))) // Force "smtp2"
        ).andExpect(status().isOk)

        assertThat(smtpServer2.waitForIncomingEmail(3_000, 1)).isTrue
        assertThat(smtpServer2.receivedMessages)
            .hasSize(1)
            .satisfies {
                val msg = it[0]
                assertThat(msg.getFromAddress()).isEqualTo(EmailInfo.EmailAddress("from@test.com", "John"))
                assertThat(msg.getTo()).isEqualTo(listOf(EmailInfo.EmailAddress("to@test.com")))
                assertThat(msg.subject).isEqualTo("My subject")
                assertThat(msg.getPlaintText()).isEqualTo("My body")
            }
        assertThat(smtpServer1.receivedMessages).isEmpty()

        val callback: NotificationCallback = callbackDeque.pollLast(5, TimeUnit.SECONDS)!!
        assertThat(callback).isEqualTo(
            NotificationSuccessCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
                    type = NotificationInfo.Type.EMAIL
                ),
                counter = 1,
                tryIndex = 0,
                provider = NotificationCallback.ProviderCallbackInfo(
                    id = "smtp2",
                    type = "smtp"
                )
            )
        )
        assertThat(callbackErrorDeque).isEmpty()
    }

    @Test
    fun testEmailAttachments() {
        val attachmentData = ClassPathResource("/kotlin.png").toByteArray()
        emailInfo = emailInfo.copy(
            attachments = listOf(
                EmailInfo.Attachment(filename = "kotlin.png", data = attachmentData),
                EmailInfo.Attachment(filename = "kotlin.jpg", data = attachmentData)
            )
        )

        // Send email
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("sirius", "sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isOk)

        assertThat(smtpServer1.waitForIncomingEmail(3_000, 1)).isTrue
        assertThat(smtpServer1.receivedMessages)
            .hasSize(1)
            .satisfies {
                val msg = it[0]
                assertThat(msg.getAttachments())
                    .hasSize(2)
                    .anyMatch { att -> att.filename == "kotlin.png" && att.contentType == "image/png" && att.data.contentEquals(attachmentData) }
                    .anyMatch { att -> att.filename == "kotlin.jpg" && att.contentType == "image/jpeg" && att.data.contentEquals(attachmentData) }
            }
    }

    @Test
    fun testSendEmailWithDeadLetter() {
        // Stop first SMTP
        smtpServer1.stop()

        // Create new user
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("sirius","sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isOk)

        assertThat(smtpServer2.waitForIncomingEmail(3_000, 1)).isTrue
        assertThat(smtpServer2.receivedMessages)
            .hasSize(1)
            .satisfies {
                val msg = it[0]
                assertThat(msg.getFromAddress()).isEqualTo(EmailInfo.EmailAddress("from@test.com", "John"))
                assertThat(msg.getTo()).isEqualTo(listOf(EmailInfo.EmailAddress("to@test.com")))
                assertThat(msg.subject).isEqualTo("My subject")
                assertThat(msg.getPlaintText()).isEqualTo("My body")
            }
        assertThat(smtpServer1.receivedMessages).isEmpty()

        val callback: NotificationCallback = callbackDeque.pollLast(5, TimeUnit.SECONDS)!!
        assertThat(callback).isEqualTo(
            NotificationSuccessCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
                    type = NotificationInfo.Type.EMAIL
                ),
                counter = 1,
                tryIndex = 1,
                provider = NotificationCallback.ProviderCallbackInfo(
                    id = "smtp2",
                    type = "smtp"
                )
            )
        )
        assertThat(callbackErrorDeque).isEmpty()
    }

    @Test
    fun testSendEmailFailure() {
        // Stop SMTP
        smtpServer1.stop()
        smtpServer2.stop()

        // Send email
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("sirius", "sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isOk)

        assertThat(smtpServer1.waitForIncomingEmail(1_000, 1)).isFalse
        assertThat(smtpServer1.receivedMessages).isEmpty()
        assertThat(smtpServer2.receivedMessages).isEmpty()

        assertThat(callbackDeque.pollLast(2, TimeUnit.SECONDS)).isNull()
        assertThat(callbackErrorDeque.pollLast(2, TimeUnit.SECONDS)).isEqualTo(
            NotificationErrorCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
                    type = NotificationInfo.Type.EMAIL
                ),
                tries = 5
            )
        )
    }

    @Test
    fun testInvalidParameters() {
        assertBadRequest(emailInfo.copy(to = emptyList()))
    }

    private fun assertBadRequest(emailInfo: EmailInfo) {
        mockMvc.perform(
            post("/api/email")
                .with(httpBasic("sirius","sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(emailInfo))
        ).andExpect(status().isBadRequest)
    }
}