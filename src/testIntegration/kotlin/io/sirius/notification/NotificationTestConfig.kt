package io.sirius.notification

import io.sirius.notification.config.RabbitConfig
import io.sirius.notification.model.NotificationErrorCallback
import io.sirius.notification.model.NotificationSuccessCallback
import io.sirius.notification.model.TrackingEvent
import io.sirius.notification.util.logger
import org.springframework.amqp.core.*
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.BlockingDeque
import java.util.concurrent.LinkedBlockingDeque

@Configuration
class NotificationTestConfig {

    private val logger = logger()

    @Autowired
    private lateinit var callbackExchange: TopicExchange

    // Queue to simulate a callback client
    @Bean
    fun callbackQueue(): Queue = QueueBuilder
        .nonDurable("test.callback")
        .autoDelete()
        .build()

    @Bean
    fun callbackErrorQueue(): Queue = QueueBuilder
        .nonDurable("test.error.callback")
        .autoDelete()
        .build()

    @Bean
    fun trackingQueue(): Queue = QueueBuilder
        .nonDurable("test.tracking")
        .autoDelete()
        .build()

    @Bean
    fun callbackBinding(): Binding = BindingBuilder
        .bind(callbackQueue())
        .to(callbackExchange)
        .with(RabbitConfig.callbackRoutingKey("*"))

    @Bean
    fun callbackErrorBinding(): Binding = BindingBuilder
        .bind(callbackErrorQueue())
        .to(callbackExchange)
        .with(RabbitConfig.errorCallbackRoutingKey("*"))

    @Bean
    fun trackingBinding(): Binding = BindingBuilder
        .bind(trackingQueue())
        .to(callbackExchange)
        .with(RabbitConfig.trackingRoutingKey("*", "*"))

    @Bean
    fun callbackDeque(): BlockingDeque<NotificationSuccessCallback> = LinkedBlockingDeque()

    @Bean
    fun callbackErrorDeque(): BlockingDeque<NotificationErrorCallback> = LinkedBlockingDeque()

    @Bean
    fun trackingDeque(): BlockingDeque<TrackingEvent> = LinkedBlockingDeque()

    @RabbitListener(queues = ["test.callback"])
    fun onCallbackReceived(callback: NotificationSuccessCallback) {
        logger.info("Callback success received: $callback")
        callbackDeque().addFirst(callback)
    }

    @RabbitListener(queues = ["test.error.callback"])
    fun onCallbackErrorReceived(callback: NotificationErrorCallback) {
        logger.info("Callback error received: $callback")
        callbackErrorDeque().addFirst(callback)
    }

    @RabbitListener(queues = ["test.tracking"])
    fun onTrackingEventReceived(event: TrackingEvent) {
        logger.info("Tracking event received: $event")
        trackingDeque().addFirst(event)
    }
}