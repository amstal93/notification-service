package io.sirius.notification

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.NotificationCallback
import io.sirius.notification.model.NotificationErrorCallback
import io.sirius.notification.model.NotificationSuccessCallback
import io.sirius.notification.model.TrackingEvent
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import java.util.concurrent.BlockingDeque
import org.springframework.boot.web.server.LocalServerPort


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension::class)
@ActiveProfiles("dev", "test")
@AutoConfigureMockMvc
abstract class AbstractTest {

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @LocalServerPort
    protected var serverPort: Int = 8080

    @Autowired
    protected lateinit var callbackDeque: BlockingDeque<NotificationSuccessCallback>

    @Autowired
    protected lateinit var callbackErrorDeque: BlockingDeque<NotificationErrorCallback>

    @Autowired
    protected lateinit var trackingQueue: BlockingDeque<TrackingEvent>

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    protected fun payload(obj: Any): String = objectMapper.writeValueAsString(obj)

}