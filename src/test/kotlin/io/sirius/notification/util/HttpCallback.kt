package io.sirius.notification.util

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import org.apache.http.HttpStatus
import java.time.Duration
import java.util.concurrent.BlockingDeque
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.TimeUnit

class HttpCallback : HttpHandler {

    private val logger = logger()
    private val queue: BlockingDeque<HttpRequest> = LinkedBlockingDeque()

    override fun handle(exchange: HttpExchange) {
        val request = HttpRequest(
            exchange.requestMethod,
            exchange.requestBody?.bufferedReader()?.use { it.readText() },
            exchange.requestHeaders
        )
        logger.info("Http request received: {}", request)
        queue.addFirst(request)
        exchange.sendResponseHeaders(HttpStatus.SC_NO_CONTENT, -1)
    }

    fun waitForResponse(timeout: Duration = Duration.ofSeconds(3)): HttpRequest {
        return queue.pollLast(timeout.seconds, TimeUnit.SECONDS)!!
    }

    data class HttpRequest(
        val method: String,
        val body: String?,
        val headers: Map<String, List<String>>
    )
}