package io.sirius.notification.util

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CredentialsTest {

    @Test
    fun testBasicAuthentication() {
        val credentials = PasswordCredentials("john", "azerty")
        assertThat(credentials.toHeader().value).isEqualTo("Basic am9objphemVydHk=")
    }

    @Test
    fun testBearerAuthentication() {
        val credentials = BearerCredentials("1234567890")
        assertThat(credentials.toHeader().value).isEqualTo("Bearer 1234567890")
    }
}