package io.sirius.notification.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import org.apache.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.time.Duration
import java.util.concurrent.BlockingDeque
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.TimeUnit

class MockEndpoint(private val objectMapper: ObjectMapper) : HttpHandler {

    private val logger = logger()
    private val queue: BlockingDeque<HttpRequest> = LinkedBlockingDeque()

    var response: HttpResponse = HttpResponse()


    override fun handle(exchange: HttpExchange) {
        val request = HttpRequest(
            exchange.requestMethod,
            exchange.requestBody?.bufferedReader()?.use { it.readText() },
            exchange.requestHeaders
        )
        logger.info("Http request received: {}", request)
        queue.addFirst(request)

        if (response.entity != null) {
            val responseData = objectMapper.writeValueAsBytes(response.entity)
            exchange.responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json")
            exchange.sendResponseHeaders(response.status.value(), responseData.size.toLong())
            exchange.responseBody.write(responseData)

        } else {
            exchange.sendResponseHeaders(response.status.value(), 0)
        }
    }

    fun waitForRequest(timeout: Duration = Duration.ofSeconds(2)): HttpRequest? =
        queue.pollLast(timeout.seconds, TimeUnit.SECONDS)

    data class HttpRequest(
        val method: String,
        val body: String?,
        val headers: Map<String, List<String>>
    ) {

        inline fun <reified T> getJsonBody(objectMapper: ObjectMapper): T = objectMapper.readValue(body, T::class.java)
    }

    data class HttpResponse(
        val status: HttpStatus = HttpStatus.NO_CONTENT,
        val entity: Any? = null
    )
}