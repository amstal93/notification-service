package io.sirius.notification.services.impl

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.sirius.notification.config.NotificationProperties
import io.sirius.notification.config.RabbitConfig.Companion.HEADER_X_DEATH
import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.services.DeadLetterService
import io.sirius.notification.util.DeathHeader
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.rabbit.core.RabbitTemplate

class DeadLetterServiceImplTest {

    private lateinit var data: ByteArray
    private lateinit var errorService: DeadLetterService
    private lateinit var rabbitTemplate: RabbitTemplate
    private lateinit var exchange: String

    @BeforeEach
    fun before() {
        exchange = "exchange.test"
        rabbitTemplate = mockk(relaxed = true)
        every { rabbitTemplate.messageConverter.fromMessage(any()) } returns DummyNotification()

        errorService = DeadLetterServiceImpl(
            rabbitTemplate,
            exchange,
            ExchangeServiceImpl(exchange, exchange, rabbitTemplate),
            NotificationProperties()
        )
        data = ByteArray(0)
    }

    @Test
    fun testFirstError() {
        val message = Message(data, createMessageProperties(1))
        errorService.onFailedNotification(message)

        verify(exactly = 1) {
            rabbitTemplate.send(
                exchange,
                "routing.key",
                Message(data, createMessageProperties(1))
            )
        }
    }

    @Test
    fun testSecondError() {
        val message = Message(data, createMessageProperties(2))
        errorService.onFailedNotification(message)

        verify(exactly = 1) {
            rabbitTemplate.send(
                exchange,
                "routing.key",
                Message(data, createMessageProperties(2))
            )
        }
    }

    private fun createMessageProperties(retriesCount: Int): MessageProperties {
        val props = MessageProperties()
        val header = DeathHeader("reason", retriesCount, "exchange", emptySet(), "queue")
        props.setHeader(HEADER_X_DEATH, listOf(header.toHeader()))
        props.receivedRoutingKey = "routing.key"
        return props
    }

    private fun DeathHeader.toHeader(): Map<String, *> = mapOf(
        "reason" to reason,
        "count" to count.toLong(),
        "exchange" to exchange,
        "queue" to queue,
        "routing-keys" to routingKeys.toList()
    )

    private class DummyNotification(
        override val id: String = "123456",
        override val metadata: Map<String, String> = emptyMap(),
        override val type: NotificationInfo.Type = NotificationInfo.Type.EMAIL
    ) : NotificationInfo
}