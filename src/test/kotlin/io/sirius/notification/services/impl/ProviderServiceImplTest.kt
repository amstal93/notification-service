package io.sirius.notification.services.impl

import io.sirius.notification.providers.NotificationProvider
import io.sirius.notification.services.ProviderService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

internal class ProviderServiceImplTest {

    private lateinit var service: ProviderService<DummyProvider>

    @BeforeEach
    fun before() {
        service = ProviderServiceImpl(
            listOf(
                DummyProvider("provider1"),
                DummyProvider("provider2"),
                DummyProvider("provider3")
            )
        )
    }

    @Test
    fun testAllProviderSelect() {
        assertThat(service.getProvider(0, emptySet())).isEqualTo(DummyProvider("provider1"))
        assertThat(service.getProvider(1, emptySet())).isEqualTo(DummyProvider("provider2"))
        assertThat(service.getProvider(2, emptySet())).isEqualTo(DummyProvider("provider3"))
        assertThat(service.getProvider(3, emptySet())).isEqualTo(DummyProvider("provider1"))
        assertThat(service.getProvider(4, emptySet())).isEqualTo(DummyProvider("provider2"))
        assertThat(service.getProvider(5, emptySet())).isEqualTo(DummyProvider("provider3"))
    }

    @Test
    fun testPartialProviderSelect() {
        val providers = setOf("provider1", "provider3")

        assertThat(service.getProvider(0, providers)).isEqualTo(DummyProvider("provider1"))
        assertThat(service.getProvider(1, providers)).isEqualTo(DummyProvider("provider3"))
        assertThat(service.getProvider(2, providers)).isEqualTo(DummyProvider("provider1"))
        assertThat(service.getProvider(3, providers)).isEqualTo(DummyProvider("provider3"))
    }

    @Test
    fun testUnknownProvider() {
        assertThrows<IllegalArgumentException> {
            service.getProvider(0, setOf("provider-unknown"))
        }
    }

    private data class DummyProvider(
        val name: String,
        override val id: String = name,
        override val provider: String = name
    ) : NotificationProvider
}