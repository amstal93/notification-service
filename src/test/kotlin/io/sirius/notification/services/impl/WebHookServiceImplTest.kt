package io.sirius.notification.services.impl

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.sun.net.httpserver.HttpServer
import io.mockk.mockk
import io.sirius.notification.model.WebHookInfo
import io.sirius.notification.services.NotificationService
import io.sirius.notification.util.HttpCallback
import io.sirius.notification.util.TestUtil
import io.sirius.notification.util.getFirst
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.amqp.rabbit.core.RabbitTemplate
import java.net.InetSocketAddress
import java.net.SocketException
import java.net.URI
import java.util.*

class WebHookServiceImplTest {

    private var port: Int = 8000
    private lateinit var httpServer: HttpServer
    private lateinit var objectMapper: ObjectMapper
    private lateinit var httpCallback: HttpCallback
    private lateinit var httpClient: CloseableHttpClient
    private lateinit var webHookService: NotificationService<WebHookInfo<*>>
    private lateinit var rabbitTemplate: RabbitTemplate

    @BeforeEach
    fun before() {
        objectMapper = TestUtil.objectMapper()
        httpClient = TestUtil.httpClient()
        rabbitTemplate = mockk(relaxed = true)

        webHookService = WebHookServiceImpl(
            httpClient,
            ExchangeServiceImpl("exchange", "exchange", rabbitTemplate),
            objectMapper
        )

        port = 8000 + Random().nextInt(2000)
        httpCallback = HttpCallback()
        httpServer = HttpServer.create(InetSocketAddress(port), -1)
        httpServer.createContext("/api", httpCallback)
        httpServer.start()
    }

    @AfterEach
    fun after() {
        httpServer.stop(0)
        httpClient.close()
    }

    @Nested
    inner class WebHookHeader {

        @Test
        fun testWebHookHeaders() {
            webHookService.processNotification(
                WebHookInfo<Unit>(
                    remoteUri = URI.create("http://localhost:$port/api"),
                    headers = listOf(WebHookInfo.Header("x-test", "my-test"))
                )
            )

            assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
                assertThat(rsp.headers.getFirst("x-test")).isEqualTo("my-test")
            }
        }
    }

    @Nested
    inner class WebHookUrl {

        @Test
        fun testInvalidWebHookUrl() {
            assertThrows<SocketException> {
                webHookService.processNotification(
                    WebHookInfo<Unit>(remoteUri = URI.create("http://localhost:8082/api"))
                )
            }
        }
    }

    @Nested
    inner class WebHookMethod {

        @Test
        fun testPostWebHook() {
            webHookService.processNotification(
                WebHookInfo<Unit>(
                    remoteUri = URI.create("http://localhost:$port/api"),
                    method = WebHookInfo.Method.POST
                )
            )

            assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
                assertThat(rsp.method).isEqualTo("POST")
            }
        }

        @Test
        fun testPutWebHook() {
            webHookService.processNotification(
                WebHookInfo<Unit>(
                    remoteUri = URI.create("http://localhost:$port/api"),
                    method = WebHookInfo.Method.PUT
                )
            )

            assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
                assertThat(rsp.method).isEqualTo("PUT")
            }
        }

        @Test
        fun testGetWebHook() {
            webHookService.processNotification(
                WebHookInfo<Unit>(
                    remoteUri = URI.create("http://localhost:$port/api"),
                    method = WebHookInfo.Method.GET
                )
            )

            assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
                assertThat(rsp.method).isEqualTo("GET")
            }
        }
    }

    @Nested
    inner class WebHookBody {

        @Test
        fun testWebHookStringBody() {
            webHookService.processNotification(
                WebHookInfo(
                    remoteUri = URI.create("http://localhost:$port/api"),
                    body = "Hello world!"
                )
            )

            assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
                assertThat(rsp.method).isEqualTo("POST")
                assertThat(rsp.body).isEqualTo("Hello world!")
            }
        }

        @Test
        fun testWebHookJsonBody() {
            webHookService.processNotification(
                WebHookInfo(
                    remoteUri = URI.create("http://localhost:$port/api"),
                    body = CustomBody("john", CustomBody.Value("john@doe.com"))
                )
            )

            assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
                assertThat(rsp.method).isEqualTo("POST")
                assertThat(rsp.body).isEqualTo("""{"name":"john","value":{"email":"john@doe.com"}}""")
            }
        }

    }

    @Nested
    inner class WebHookInfoSerialization {

        @Test
        fun testStringSerialization() {
            val entity = WebHookInfo(
                id = "123456",
                remoteUri = URI.create("http://localhots:$port/api"),
                method = WebHookInfo.Method.POST,
                headers = listOf(WebHookInfo.Header("x-test", "value")),
                body = "Hello world!"
            )

            val serializedEntity = objectMapper.writeValueAsString(entity)
            assertThat(serializedEntity).isEqualTo("""{"id":"123456","metadata":{},"remoteUri":"http://localhots:$port/api","method":"POST","headers":[{"name":"x-test","value":"value"}],"body":"Hello world!"}""")

            val deserializedEntity = objectMapper.readValue<WebHookInfo<String>>(serializedEntity)
            assertThat(entity).isEqualTo(deserializedEntity)
        }

        @Test
        fun testJsonSerialization() {
            val entity = WebHookInfo(
                id = "123456",
                remoteUri = URI.create("http://localhots:$port/api"),
                method = WebHookInfo.Method.POST,
                headers = listOf(WebHookInfo.Header("x-test", "value")),
                body = CustomBody("john", CustomBody.Value("john@doe.com"))
            )

            val serializedEntity = objectMapper.writeValueAsString(entity)
            assertThat(serializedEntity).isEqualTo("""{"id":"123456","metadata":{},"remoteUri":"http://localhots:$port/api","method":"POST","headers":[{"name":"x-test","value":"value"}],"body":{"name":"john","value":{"email":"john@doe.com"}}}""")

            val deserializedEntity = objectMapper.readValue<WebHookInfo<CustomBody>>(serializedEntity)
            assertThat(entity).isEqualTo(deserializedEntity)
        }
    }

    internal data class CustomBody(
        val name: String,
        val value: Value
    ) {
        data class Value(val email: String)
    }

}