package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpServer
import io.sirius.notification.AccessRestrictedException
import io.sirius.notification.EntityTooLargeException
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.util.*
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.net.InetSocketAddress
import java.net.URI
import java.util.*

class EsendexProviderTest {

    private lateinit var objectMapper: ObjectMapper
    private lateinit var httpClient: CloseableHttpClient
    private lateinit var provider: EsendexProvider

    @BeforeEach
    fun before() {
        httpClient = TestUtil.httpClient()
        objectMapper = TestUtil.objectMapper()
    }

    @AfterEach
    fun after() {
        httpClient.close()
    }

    @Nested
    inner class RealProvider {

        @BeforeEach
        fun before() {
            val config = EsendexProviderConfig(
                provider = "Esendex",
                account = System.getProperty("esendex_account"),
                login = System.getProperty("esendex_login"),
                password = System.getProperty("esendex_password")
            )
            provider = config.createProvider(
                httpClient,
                objectMapper
            )
        }

        @Disabled // Because costs money!
        @Test
        fun testSendSms() {
            provider.sendSms(
                SmsInfo(
                    from = "test",
                    phoneNumber = System.getProperty("esendex_phone"),
                    message = "Message test é@ù"
                )
            )
        }
    }

    @Nested
    inner class MockProvider {

        private lateinit var smsInfo: SmsInfo
        private lateinit var httpServer: HttpServer
        private lateinit var endpoint: MockEndpoint

        @BeforeEach
        fun before() {
            endpoint = MockEndpoint(objectMapper)
            val port = 8000 + Random().nextInt(2000)

            httpServer = HttpServer.create(InetSocketAddress(port), 0)
            httpServer.createContext("/send/sms", endpoint)
            httpServer.start()

            provider = EsendexProvider(
                provider = "Esendex",
                httpClient = httpClient,
                objectMapper = objectMapper,
                serverUri = URI.create("http://localhost:$port/send/sms"),
                credentials = PasswordCredentials("login", "pwd"),
                account = "EX123456"
            )

            smsInfo = SmsInfo(
                from = "test",
                phoneNumber = "0606060606",
                message = "Message test"
            )
        }

        @AfterEach
        fun after() {
            httpServer.stop(0)
        }

        @Test
        fun testSmsSending() {
            endpoint.response = MockEndpoint.HttpResponse(
                HttpStatus.OK,
                EsendexProvider.EsendexResult(
                    batch = EsendexProvider.EsendexResult.Batch("123456", emptyList())
                )
            )

            provider.sendSms(smsInfo)

            assertThat(endpoint.waitForRequest()!!).satisfies { request ->
                assertThat(request.headers.getFirst(HttpHeaders.AUTHORIZATION)).isEqualTo("Basic bG9naW46cHdk")
                assertThat(request.getJsonBody<EsendexProvider.EsendexRequest>(objectMapper))
                    .isEqualTo(
                        EsendexProvider.EsendexRequest(
                            accountreference = "EX123456",
                            messages = listOf(
                                EsendexProvider.EsendexRequest.Message(
                                    from = "test",
                                    to = "0606060606",
                                    body = "Message test"
                                )
                            )
                        )
                    )
            }
        }

        @Test
        fun testForbidden() {
            endpoint.response = MockEndpoint.HttpResponse(HttpStatus.FORBIDDEN)
            assertThrows<AccessRestrictedException> { provider.sendSms(smsInfo) }
        }

        @Test
        fun testFailure() {
            endpoint.response = MockEndpoint.HttpResponse(
                HttpStatus.INTERNAL_SERVER_ERROR,
                EsendexProvider.EsendexResult(
                    errors = listOf(EsendexProvider.EsendexResult.Error("12345", "Server has crashed"))
                )
            )

            val ex = assertThrows<HttpException> { provider.sendSms(smsInfo) }
            assertThat(ex.status).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
            assertThat(ex.message).isEqualTo("Esendex error (Server has crashed)")
        }
    }

    @Nested
    inner class SmsCount {

        @BeforeEach
        fun before() {
            provider = EsendexProvider(
                provider = "Esendex",
                httpClient = httpClient,
                objectMapper = objectMapper,
                serverUri = URI.create("http://localhost:1234/send/sms"),
                credentials = PasswordCredentials("login", "pwd"),
                account = "EX123456"
            )
        }

        @Test
        fun testSmsCharsCount() {
            assertThat(provider.getSmsCharsCount("azerty")).isEqualTo(6)
            assertThat(provider.getSmsCharsCount("130 €")).isEqualTo(6)
            assertThat(provider.getSmsCharsCount("^{},[~]|€")).isEqualTo(18)
        }

        @Test
        fun testSmsCount() {
            // 50 chars
            assertThat(provider.getSmsCount(generateString(50))).isEqualTo(1)

            // 160 chars
            assertThat(provider.getSmsCount(generateString(160))).isEqualTo(1)

            // 160 chars with special char
            assertThat(provider.getSmsCount(generateString(159) + "€")).isEqualTo(2)

            // 190 chars
            assertThat(provider.getSmsCount(generateString(190))).isEqualTo(2)

            // 310 chars
            assertThat(provider.getSmsCount(generateString(310))).isEqualTo(3)

            // 500 chars
            assertThat(provider.getSmsCount(generateString(500))).isEqualTo(4)

            // Too large
            assertThrows<EntityTooLargeException> { provider.getSmsCount(generateString(613)) }
        }

        private fun generateString(length: Int): String {
            val builder = StringBuilder()
            for (i in 1..length) {
                builder.append("a")
            }
            return builder.toString()
        }
    }


}