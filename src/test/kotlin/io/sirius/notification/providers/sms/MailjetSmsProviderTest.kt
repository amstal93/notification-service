package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpServer
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.util.*
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.net.InetSocketAddress
import java.net.URI
import java.util.*

class MailjetSmsProviderTest {

    private lateinit var smsInfo: SmsInfo
    private lateinit var httpServer: HttpServer
    private lateinit var endpoint: MockEndpoint
    private lateinit var objectMapper: ObjectMapper
    private lateinit var httpClient: CloseableHttpClient
    private lateinit var provider: MailjetSmsProvider

    @BeforeEach
    fun before() {
        httpClient = TestUtil.httpClient()
        objectMapper = TestUtil.objectMapper()

        endpoint = MockEndpoint(objectMapper)
        val port = 8000 + Random().nextInt(10000)

        httpServer = HttpServer.create(InetSocketAddress(port), 0)
        httpServer.createContext("/send/sms", endpoint)
        httpServer.start()

        provider = MailjetSmsProvider(
            provider = "Mailjet",
            httpClient = httpClient,
            objectMapper = objectMapper,
            credentials = BearerCredentials("123456"),
            serverUri = URI.create("http://localhost:$port/send/sms"),
        )

        smsInfo = SmsInfo(
            from = "test",
            phoneNumber = "0606060606",
            message = "Message test"
        )
    }

    @AfterEach
    fun after() {
        httpClient.close()
        httpServer.stop(0)
    }

    @Test
    fun testSmsSending() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.OK,
            MailjetSmsProvider.MailjetResult(2, "123456")
        )

        val smsCount = provider.sendSms(smsInfo)
        assertThat(smsCount).isEqualTo(2)

        assertThat(endpoint.waitForRequest()!!).satisfies { request ->
            assertThat(request.headers.getFirst(HttpHeaders.AUTHORIZATION)).isEqualTo("Bearer 123456")
            assertThat(request.getJsonBody<MailjetSmsProvider.MailjetSms>(objectMapper))
                .isEqualTo(
                    MailjetSmsProvider.MailjetSms(
                        From = "test",
                        To = "0606060606",
                        Text = "Message test"
                    )
                )
        }
    }

    @Test
    fun testSmsFailure() {
        endpoint.response = MockEndpoint.HttpResponse(HttpStatus.FORBIDDEN)
        assertThrows<HttpException> { provider.sendSms(smsInfo) }
    }
}