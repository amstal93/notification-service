package io.sirius.notification.providers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.readValue
import io.sirius.notification.providers.emailing.MailjetProviderConfig
import io.sirius.notification.providers.emailing.MandrillProviderConfig
import io.sirius.notification.providers.emailing.SendGridProviderConfig
import io.sirius.notification.providers.emailing.SmtpProviderConfig
import io.sirius.notification.providers.sms.AllMySmsProviderConfig
import io.sirius.notification.providers.sms.DummySmsProviderConfig
import io.sirius.notification.providers.sms.EsendexProviderConfig
import io.sirius.notification.providers.sms.MailjetSmsProviderConfig
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

class ProvidersDescriptorTest {

    private lateinit var mapper: ObjectMapper

    @BeforeEach
    fun before() {
        mapper = Jackson2ObjectMapperBuilder().factory(YAMLFactory()).build()
    }

    @Test
    fun testDescriptorParsing() {
        val descriptor: ProvidersDescriptor = mapper.readValue(javaClass.getResource("providers.yml"))

        assertThat(descriptor.emailing)
            .hasSize(5)
            .isEqualTo(
                listOf(
                    SmtpProviderConfig(provider = "smtp", id = "gmail", host = "smtp.gmail.com", port = 567, username = "fabar", password = "pwd"),
                    SmtpProviderConfig(provider = "smtp", id = "hotmail", host = "smtp.hotmail.com", port = 567, username = "fabar", password = "pwd"),
                    MailjetProviderConfig(provider = "mailjet", apiKey = "fabar", secretKey = "pwd"),
                    SendGridProviderConfig(provider = "sendGrid", apiKey = "123456"),
                    MandrillProviderConfig(provider = "mandrill", apiKey = "123456", subAccount = "sub")
                )
            )

        assertThat(descriptor.sms)
            .hasSize(4)
            .isEqualTo(
                listOf(
                    EsendexProviderConfig(provider = "esendex", account = "EX123456", login = "fabar", password = "pwd"),
                    AllMySmsProviderConfig(provider = "allMySms", login = "fabar", apiKey = "pwd"),
                    MailjetSmsProviderConfig(provider = "mailjetSms", token = "fabar-token"),
                    DummySmsProviderConfig(provider = "dummySms", host = "localhost", port = 25)
                )
            )

    }
}