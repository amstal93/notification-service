package io.sirius.notification.providers.emailing

import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.util.MimeMessageExt.getAttachments
import io.sirius.notification.util.MimeMessageExt.getBcc
import io.sirius.notification.util.MimeMessageExt.getCc
import io.sirius.notification.util.MimeMessageExt.getFromAddress
import io.sirius.notification.util.MimeMessageExt.getHtmlText
import io.sirius.notification.util.MimeMessageExt.getPlaintText
import io.sirius.notification.util.MimeMessageExt.getTo
import io.sirius.notification.util.MimeMessageExt.isMultipart
import io.mockk.mockk
import io.sirius.notification.util.toByteArray
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.core.io.ClassPathResource
import org.springframework.mail.MailAuthenticationException

class SmtpEmailingProviderTest {

    @Nested
    inner class NotAuthenticatedSmtp {

        private lateinit var smtpProvider: SmtpEmailingProvider
        private lateinit var smtpServer: GreenMail

        @BeforeEach
        fun before() {
            smtpServer = GreenMail(ServerSetup(25000, null, ServerSetup.PROTOCOL_SMTP))
            smtpServer.start()

            val config = SmtpProviderConfig(provider = "test",host = "localhost", port = 25000)
            smtpProvider = config.createProvider(mockk(), mockk())
        }

        @AfterEach
        fun after() {
            smtpServer.stop()
        }

        @Test
        fun testPlainTextEmail() {
            smtpProvider.sendEmail(
                EmailInfo(
                    from = EmailInfo.EmailAddress("from@test.com", "John"),
                    to = listOf(EmailInfo.EmailAddress("to@test.com")),
                    subject = "My subject é@",
                    plainText = "My body %@é'[ç"
                )
            )

            // Check received mail
            assertThat(smtpServer.receivedMessages)
                .hasSize(1)
                .satisfies {
                    val msg = it[0]
                    assertThat(msg.getFromAddress()).isEqualTo(EmailInfo.EmailAddress("from@test.com", "John"))
                    assertThat(msg.getTo()).isEqualTo(listOf(EmailInfo.EmailAddress("to@test.com")))
                    assertThat(msg.getCc()).isEmpty()
                    assertThat(msg.getBcc()).isEmpty()
                    assertThat(msg.subject).isEqualTo("My subject é@")
                    assertThat(msg.isMultipart()).isFalse
                    assertThat(msg.getPlaintText()).isEqualTo("My body %@é'[ç")
                    assertThat(msg.getHtmlText()).isNull()
                    assertThat(msg.getAttachments()).isEmpty()
                }
        }

        @Test
        fun testHtmlEmail() {
            smtpProvider.sendEmail(
                EmailInfo(
                    from = EmailInfo.EmailAddress("from@test.com", "John"),
                    to = listOf(EmailInfo.EmailAddress("to@test.com")),
                    subject = "My subject é@",
                    htmlText = "<html>My body %@é'[ç</html>"
                )
            )

            // Check received mail
            assertThat(smtpServer.receivedMessages)
                .hasSize(1)
                .satisfies {
                    val msg = it[0]
                    assertThat(msg.isMultipart()).isFalse
                    assertThat(msg.getPlaintText()).isNull()
                    assertThat(msg.getHtmlText()).isEqualTo("<html>My body %@é'[ç</html>")
                }
        }

        @Test
        fun testHtmlAndPlainEmail() {
            smtpProvider.sendEmail(
                EmailInfo(
                    from = EmailInfo.EmailAddress("from@test.com", "John"),
                    to = listOf(EmailInfo.EmailAddress("to@test.com")),
                    subject = "My subject é@",
                    plainText = "My body %@é'[ç",
                    htmlText = "<html>My body %@é'[ç</html>"
                )
            )

            // Check received mail
            assertThat(smtpServer.receivedMessages)
                .hasSize(1)
                .satisfies {
                    val msg = it[0]
                    assertThat(msg.isMultipart()).isTrue
                    assertThat(msg.getPlaintText()).isEqualTo("My body %@é'[ç")
                    assertThat(msg.getHtmlText()).isEqualTo("<html>My body %@é'[ç</html>")
                }
        }

        @Test
        fun testEmailAttachments() {
            smtpProvider.sendEmail(
                EmailInfo(
                    from = EmailInfo.EmailAddress("from@test.com", "John"),
                    to = listOf(EmailInfo.EmailAddress("to@test.com")),
                    subject = "My subject",
                    plainText = "My body",
                    attachments = listOf(
                        EmailInfo.Attachment(
                            "image.png",
                            "image/jpeg",
                            ClassPathResource("/io/sirius/notification/providers/kotlin.png").toByteArray()
                        ),
                        EmailInfo.Attachment(
                            "image2.png",
                            "image/png",
                            ClassPathResource("/io/sirius/notification/providers/kotlin.png").toByteArray()
                        )
                    )
                )
            )

            // Check received mail
            assertThat(smtpServer.receivedMessages)
                .hasSize(1)
                .satisfies {
                    val msg = it[0]
                    assertThat(msg.isMultipart()).isTrue
                    assertThat(msg.getPlaintText()).isEqualTo("My body")
                    assertThat(msg.getHtmlText()).isNull()
                    assertThat(msg.getAttachments())
                        .hasSize(2)
                        .anyMatch { attachment -> attachment.filename == "image.png" }
                        .anyMatch { attachment -> attachment.filename == "image2.png" }
                }
        }

    }

    @Nested
    inner class AuthenticatedSmtp {

        private lateinit var smtpServer: GreenMail

        @BeforeEach
        fun before() {
            smtpServer = GreenMail(ServerSetup(25000, null, "smtp"))
            smtpServer.setUser("my-login", "my-password")
            smtpServer.start()
        }

        @AfterEach
        fun after() {
            smtpServer.stop()
        }

        @Test
        fun testValidLogin() {
            val config = SmtpProviderConfig(provider = "test", host = "localhost", port = 25000, username = "my-login", password = "my-password")
            val smtpProvider = config.createProvider(mockk(), mockk())

            smtpProvider.sendEmail(
                EmailInfo(
                    from = EmailInfo.EmailAddress("from@test.com", "John"),
                    to = listOf(EmailInfo.EmailAddress("to@test.com")),
                    subject = "My subject",
                    plainText = "My body"
                )
            )

            // Check received mail
            assertThat(smtpServer.receivedMessages)
                .hasSize(1)
                .satisfies {
                    val msg = it[0]
                    assertThat(msg.getFromAddress()).isEqualTo(EmailInfo.EmailAddress("from@test.com", "John"))
                    assertThat(msg.getTo()).isEqualTo(listOf(EmailInfo.EmailAddress("to@test.com")))
                    assertThat(msg.subject).isEqualTo("My subject")
                    assertThat(msg.getPlaintText()).isEqualTo("My body")
                }
        }

        @Test
        fun testInvalidLogin() {
            val config = SmtpProviderConfig(provider = "test", host = "localhost", port = 25000, username = "wrong-login", password = "wrong-password")
            val smtpProvider = config.createProvider(mockk(), mockk())

            assertThrows<MailAuthenticationException> {
                smtpProvider.sendEmail(
                    EmailInfo(
                        from = EmailInfo.EmailAddress("from@test.com", "John"),
                        to = listOf(EmailInfo.EmailAddress("to@test.com")),
                        subject = "My subject",
                        plainText = "My body"
                    )
                )
            }

            // Check received mail
            assertThat(smtpServer.receivedMessages).isEmpty()
        }
    }
}