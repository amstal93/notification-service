package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpServer
import io.sirius.notification.NotificationSendingException
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.MockEndpoint
import io.sirius.notification.util.TestUtil
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.http.HttpStatus
import java.net.InetSocketAddress
import java.net.URI
import java.util.*

class MandrillProviderTest {

    private lateinit var objectMapper: ObjectMapper
    private lateinit var httpClient: CloseableHttpClient

    private lateinit var httpServer: HttpServer
    private lateinit var endpoint: MockEndpoint
    private lateinit var emailInfo: EmailInfo
    private lateinit var provider: MandrillProvider

    @BeforeEach
    fun before() {
        httpClient = TestUtil.httpClient()
        objectMapper = TestUtil.objectMapper()

        endpoint = MockEndpoint(objectMapper)
        val port = 8000 + Random().nextInt(2000)

        httpServer = HttpServer.create(InetSocketAddress(port), 0)
        httpServer.createContext("/send/email", endpoint)
        httpServer.start()

        provider = MandrillProvider(
            provider = "Mandrill",
            httpClient = httpClient,
            objectMapper = objectMapper,
            serverUri = URI.create("http://localhost:$port/send/email"),
            apiKey = "key-123456",
            subAccount = null
        )

        emailInfo = EmailInfo(
            from = EmailInfo.EmailAddress("john@doe.com", "John"),
            to = listOf(EmailInfo.EmailAddress("to@test.com")),
            subject = "My subject",
            plainText = "My body",
            htmlText = "My HTML"
        )
    }

    @AfterEach
    fun after() {
        httpClient.close()
        httpServer.stop(0)
    }

    @Test
    fun testSending() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.OK,
            listOf(
                MandrillProvider.MandrillRecipientResponse(
                    email = "to@test.com",
                    status = MandrillProvider.MandrillRecipientResponse.Status.sent,
                    reject_reason = null,
                    "123456"
                )
            )
        )

        provider.sendEmail(emailInfo)

        assertThat(endpoint.waitForRequest()!!).satisfies { request ->
            assertThat(request.getJsonBody<MandrillProvider.MandrillPayload>(objectMapper)).isEqualTo(
                MandrillProvider.MandrillPayload(
                    key = "key-123456",
                    message = MandrillProvider.MandrillMessage(
                        html = "My HTML",
                        text = "My body",
                        subject = "My subject",
                        from_email = "john@doe.com",
                        from_name = "John",
                        to = listOf(
                            MandrillProvider.MandrillMessage.Recipient("to@test.com", null, "to")
                        ),
                        attachments = listOf(),
                        subaccount = null,
                        metadata = ProviderMetadata(emailInfo.id, "Mandrill", "Mandrill")
                    )
                )
            )
        }
    }

    @Test
    fun testSendingFailure() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.OK,
            listOf(
                MandrillProvider.MandrillRecipientResponse(
                    email = "to@test.com",
                    status = MandrillProvider.MandrillRecipientResponse.Status.invalid,
                    reject_reason = "rejected",
                    "123456"
                )
            )
        )

        assertThrows<NotificationSendingException> {
            provider.sendEmail(emailInfo)
        }
    }

    @Test
    fun testHttpFailure() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.BAD_REQUEST,
            MandrillProvider.MandrillError(
                status = "error",
                code = 123,
                name = "error",
                message = "error message"
            )
        )

        assertThrows<HttpException> {
            provider.sendEmail(emailInfo)
        }
    }

}