package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpServer
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.util.*
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.net.InetSocketAddress
import java.net.URI
import java.util.*

class MailjetEmailingProviderTest {

    private lateinit var objectMapper: ObjectMapper
    private lateinit var httpClient: CloseableHttpClient

    @BeforeEach
    fun before() {
        httpClient = TestUtil.httpClient()
        objectMapper = TestUtil.objectMapper()
    }

    @AfterEach
    fun after() {
        httpClient.close()
    }

    @Nested
    inner class RealProvider {

        private lateinit var emailInfo: EmailInfo
        private lateinit var provider: MailjetEmailingProvider

        @BeforeEach
        fun before() {
            val config = MailjetProviderConfig(
                provider = "maijet",
                apiKey = System.getProperty("mailjet_apiKey"),
                secretKey = "mailjet_secretKey"
            )
            provider = config.createProvider(httpClient, objectMapper)

            val from = System.getProperty("mailjet_from")
            val to = System.getProperty("mailjet_to")
            emailInfo = EmailInfo(
                from = EmailInfo.EmailAddress(from, "John"),
                to = listOf(EmailInfo.EmailAddress(to)),
                subject = "My subject",
                plainText = "My body"
            )
        }

        @Disabled // Cost money!
        @Test
        fun testEmailSending() {
            provider.sendEmail(emailInfo)
        }
    }

    @Nested
    inner class MockProvider {

        private lateinit var httpServer: HttpServer
        private lateinit var endpoint: MockEndpoint
        private lateinit var emailInfo: EmailInfo
        private lateinit var provider: MailjetEmailingProvider

        @BeforeEach
        fun before() {
            endpoint = MockEndpoint(objectMapper)
            val port = 8000 + Random().nextInt(2000)

            httpServer = HttpServer.create(InetSocketAddress(port), 0)
            httpServer.createContext("/send/email", endpoint)
            httpServer.start()

            provider = MailjetEmailingProvider(
                provider = "Mailjet",
                httpClient = httpClient,
                objectMapper = objectMapper,
                credentials = PasswordCredentials("login", "mdp"),
                serverUri = URI.create("http://localhost:$port/send/email")
            )

            emailInfo = EmailInfo(
                from = EmailInfo.EmailAddress("john@doe.com", "John"),
                to = listOf(EmailInfo.EmailAddress("to@test.com")),
                subject = "My subject",
                plainText = "My body",
                htmlText = "My HTML"
            )
        }

        @AfterEach
        fun after() {
            httpServer.stop(0)
        }

        @Test
        fun testSending() {
            endpoint.response = MockEndpoint.HttpResponse(HttpStatus.NO_CONTENT)

            provider.sendEmail(emailInfo)

            assertThat(endpoint.waitForRequest()!!).satisfies { request ->
                assertThat(request.headers.getFirst(HttpHeaders.AUTHORIZATION)).isEqualTo("Basic bG9naW46bWRw")
                assertThat(request.getJsonBody<MailjetEmailingProvider.MailjetMessages>(objectMapper)).isEqualTo(
                    MailjetEmailingProvider.MailjetMessages(
                        listOf(
                            MailjetEmailingProvider.MailjetMessage(
                                From = MailjetEmailingProvider.MailjetMessage.Address("john@doe.com", "John"),
                                To = listOf(MailjetEmailingProvider.MailjetMessage.Address("to@test.com")),
                                Subject = "My subject",
                                TextPart = "My body",
                                HTMLPart = "My HTML",
                                EventPayload = objectMapper.writeValueAsString(
                                    ProviderMetadata(emailInfo.id, "Mailjet", "Mailjet")
                                )
                            )
                        )
                    )
                )
            }
        }

        @Test
        fun testFailure() {
            endpoint.response = MockEndpoint.HttpResponse(
                HttpStatus.FORBIDDEN,
                MailjetEmailingProvider.MailjetError("msg", null)
            )

            val ex = assertThrows<HttpException> { provider.sendEmail(emailInfo) }
            assertThat(ex.status).isEqualTo(HttpStatus.FORBIDDEN)
        }
    }
}