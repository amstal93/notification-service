package io.sirius.notification.controller

import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.model.WebHookInfo
import io.sirius.notification.model.WebSocketInfo
import io.sirius.notification.services.ExchangeService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class ApiController(
    private val exchangeService: ExchangeService
) {

    @PostMapping("email")
    fun submitEmail(@RequestBody @Valid request: EmailInfo) =
        exchangeService.submitNotification(request)

    @PostMapping("sms")
    fun submitSms(@RequestBody @Valid request: SmsInfo) =
        exchangeService.submitNotification(request)

    @PostMapping("webhook")
    fun submitWebhook(@RequestBody @Valid request: WebHookInfo<*>) =
        exchangeService.submitNotification(request)

    @PostMapping("websocket")
    fun submitWebSocket(@RequestBody @Valid request: WebSocketInfo) =
        exchangeService.submitNotification(request)
}