package io.sirius.notification.controller

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.InvalidParameterException
import io.sirius.notification.model.MailjetEvent
import io.sirius.notification.model.MandrillEvent
import io.sirius.notification.model.SendGridEvent
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.util.logger
import io.sirius.notification.util.readList
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/tracking")
class TrackingController(
    private val exchangeService: ExchangeService,
    private val objectMapper: ObjectMapper
) {

    private val logger = logger()

    @PostMapping(path = ["email/mandrill"], consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    fun mandrillTracking(@RequestParam("mandrill_events") body: String) {
        try {
            val events: List<MandrillEvent> = objectMapper.readList(body)
            events.forEach { exchangeService.sendTrackingEvent(it.toTrackingEvent()) }

        } catch (ex: JsonProcessingException) {
            throw InvalidParameterException("Invalid mandrill webhook", "mandrill_events", ex)
        }
    }

    /**
     * This API is used when creating a new webhook in Mandrill console.
     * When you do do this, Mandrill send a HEAD request to check if the endpoint exists
     */
    @RequestMapping(path = ["email/mandrill"], method = [RequestMethod.HEAD])
    fun mandrillPing() {
        logger.debug("New ping received from Mandrill")
    }

    @PostMapping("email/mailjet")
    fun mailjetTracking(@RequestBody events: List<MailjetEvent>) {
        events.forEach { exchangeService.sendTrackingEvent(it.toTrackingEvent(objectMapper)) }
    }

    @PostMapping("email/sendgrid")
    fun sendGridTracking(@RequestBody events: List<SendGridEvent>) {
        events.mapNotNull { it.toTrackingEvent() }
            .forEach { exchangeService.sendTrackingEvent(it) }
    }
}