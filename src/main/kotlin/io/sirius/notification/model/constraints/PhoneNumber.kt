package io.sirius.notification.model.constraints

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass


@MustBeDocumented
@Constraint(validatedBy = [PhoneNumberValidator::class])
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class PhoneNumber(
    val message: String = "Invalid phone number format",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<Payload>> = []
)

class PhoneNumberValidator : ConstraintValidator<PhoneNumber, String> {

    companion object {
        private val regex = Regex("""^\+[0-9]{3,}$""")
    }

    override fun initialize(contactNumber: PhoneNumber) {}

    override fun isValid(
        value: String?,
        cxt: ConstraintValidatorContext
    ): Boolean {
        val valid = value != null && value.isNotEmpty() && value.matches(regex)
        if (!valid) {
            return false
        }

        return try {
            val phoneUtil = PhoneNumberUtil.getInstance()
            val proto = phoneUtil.parse(value, null)
            if (phoneUtil.getNumberType(proto) != PhoneNumberUtil.PhoneNumberType.MOBILE) {
                false
            } else {
                phoneUtil.isValidNumber(proto)
            }
        } catch (ex: NumberParseException) {
            false
        }
    }
}