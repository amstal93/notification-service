package io.sirius.notification.model

import java.net.URI
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class WebHookInfo<T>(
    @field:Size(max = 64)
    override val id: String = UUID.randomUUID().toString(),

    override val metadata: Map<String, String> = emptyMap(),

    @field:NotNull
    val remoteUri: URI,

    @field:NotNull
    val method: Method = Method.POST,

    val headers: List<Header> = emptyList(),
    val body: T? = null

) : NotificationInfo {

    override val type = NotificationInfo.Type.WEBHOOK

    enum class Method { POST, GET, PUT }

    data class Header(
        @field:NotBlank
        @field:Pattern(regexp = """^X-[a-zA-Z-]+$""", flags = [Pattern.Flag.CASE_INSENSITIVE])
        @field:Size(min = 1, max = 64)
        val name: String,

        @field:NotBlank
        @field:Size(min = 1, max = 400)
        val value: String
    )

}