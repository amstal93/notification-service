package io.sirius.notification.model

import java.time.Instant

/**
 * Entity pushed in callback exchange
 */
interface NotificationCallback {
    val notification: NotificationCallbackInfo
    val provider: ProviderCallbackInfo?
    val metadata: Map<String, String>

    data class NotificationCallbackInfo(
        val id: String,
        val type: NotificationInfo.Type
    )

    data class ProviderCallbackInfo(
        val id: String,
        val type: String
    )
}

data class NotificationSuccessCallback(
    override val notification: NotificationCallback.NotificationCallbackInfo,
    override val provider: NotificationCallback.ProviderCallbackInfo?,
    override val metadata: Map<String, String> = emptyMap(),
    val tryIndex: Int,
    val counter: Int = 1
) : NotificationCallback

data class NotificationErrorCallback(
    override val notification: NotificationCallback.NotificationCallbackInfo,
    override val metadata: Map<String, String> = emptyMap(),
    val tries: Int
) : NotificationCallback {

    override val provider: NotificationCallback.ProviderCallbackInfo? = null
}

data class TrackingEvent(
    override val notification: NotificationCallback.NotificationCallbackInfo,
    override val provider: NotificationCallback.ProviderCallbackInfo?,
    override val metadata: Map<String, String>,
    val eventType: Type,
    val timestamp: Instant,
    val ipAddress: String?,
    val userAgent: String?,
    val email: String,
    val clickedUrl: String?
) : NotificationCallback {

    enum class Type { SENT, OPEN, CLICK, BOUNCE, BLOCKED, SPAM, UNSUB, DEFERRED }
}

