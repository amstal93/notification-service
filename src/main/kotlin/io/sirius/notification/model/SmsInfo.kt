package io.sirius.notification.model

import io.sirius.notification.model.constraints.PhoneNumber
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class SmsInfo(
    @field:Size(max = 64)
    override val id: String = UUID.randomUUID().toString(),

    override val metadata: Map<String, String> = emptyMap(),

    @field:NotBlank
    @field:PhoneNumber
    val phoneNumber: String,

    @field:NotBlank
    @field:Pattern(regexp = "^[a-zA-Z0-9-_ ]+$")
    val from: String,

    @field:NotBlank
    @field:Size(min = 1, max = 500)
    val message: String,

    val providers: Set<String> = emptySet()

) : NotificationInfo {

    override val type = NotificationInfo.Type.SMS
}