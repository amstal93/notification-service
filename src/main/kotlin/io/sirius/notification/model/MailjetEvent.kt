package io.sirius.notification.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.module.kotlin.readValue
import io.sirius.notification.model.databind.TimestampDeserializer
import io.sirius.notification.model.databind.TimestampSerializer
import io.sirius.notification.util.toProviderCallbackInfo
import java.time.Instant

/**
 * https://dev.mailjet.com/email/guides/webhooks/
 */
data class MailjetEvent(
    val event: Type,
    @JsonDeserialize(using = TimestampDeserializer::class)
    @JsonSerialize(using = TimestampSerializer::class)
    val time: Instant,
    val email: String,
    val ip: String?,
    @JvmField val Payload: String,
    val agent: String?,
    val url: String?
) {

    fun toTrackingEvent(objectMapper: ObjectMapper): TrackingEvent {
        val providerMetadata = objectMapper.readValue<ProviderMetadata>(Payload)

        return TrackingEvent(
            notification = NotificationCallback.NotificationCallbackInfo(
                id = providerMetadata.notificationId,
                type = NotificationInfo.Type.EMAIL
            ),
            metadata = providerMetadata.metadata,
            eventType = event.trackingType,
            provider = providerMetadata.toProviderCallbackInfo(),
            timestamp = time,
            ipAddress = ip,
            userAgent = agent,
            email = email,
            clickedUrl = url
        )
    }

    enum class Type(val trackingType: TrackingEvent.Type) {
        sent(TrackingEvent.Type.SENT),
        `open`(TrackingEvent.Type.OPEN),
        click(TrackingEvent.Type.CLICK),
        bounce(TrackingEvent.Type.BOUNCE),
        blocked(TrackingEvent.Type.BLOCKED),
        spam(TrackingEvent.Type.SPAM),
        unsub(TrackingEvent.Type.UNSUB)
    }
}