package io.sirius.notification.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.core.io.Resource

@ConfigurationProperties("notification")
class NotificationProperties {

    var maxTriesCount: Int = 5
    lateinit var descriptorPath: Resource
    var rabbit: Rabbit = Rabbit()
    var stomp: Stomp = Stomp()
    var api: Api = Api()
    var http: Http = Http()

    class Rabbit {
        var prefix: String = "sirius"
        var autoDelete: Boolean = false
    }

    class Stomp {
        var host: String = "localhost"
        var port: Int = 61613
        var login: String = "guest"
        var password: String = "guest"
    }

    class Api {
        var user: String = "sirius"
        var password: String = "sirius"
    }

    class Http {
        var connectTimeout: Int = 5_000
        var readTimeout: Int = 60_000
        var maxConnexionPerRoute: Int = 10
    }
}