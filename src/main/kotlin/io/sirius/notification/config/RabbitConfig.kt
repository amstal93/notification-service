package io.sirius.notification.config

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.util.RabbitCallback
import io.sirius.notification.util.toRabbitToken
import org.springframework.amqp.core.*
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class RabbitConfig(private val properties: NotificationProperties) {

    companion object {
        const val HEADER_X_DEATH = "x-death"

        fun notificationRoutingKey(
            notificationType: String
        ): String = "notification.$notificationType"

        fun callbackRoutingKey(
            notificationType: String
        ): String = "notification.$notificationType.success"

        fun errorCallbackRoutingKey(
            notificationType: String
        ): String = "notification.$notificationType.error"

        fun trackingRoutingKey(
            notificationType: String,
            trackingType: String
        ): String = "notification.$notificationType.tracking.$trackingType"
    }

    private val prefix = properties.rabbit.prefix

    ////////////// EXCHANGES //////////////

    @Bean
    fun notificationExchangeName(): String = "$prefix.notification"

    @Bean
    fun notificationCallbackExchangeName(): String = "$prefix.notification.callback"

    @Bean
    fun notificationExchange(): DirectExchange = ExchangeBuilder
        .directExchange(notificationExchangeName())
        .configureAndBuild()

    @Bean
    fun callbackExchange(): TopicExchange = ExchangeBuilder
        .topicExchange(notificationCallbackExchangeName())
        .configureAndBuild()

    @Bean
    fun deadLetterExchange(): FanoutExchange = ExchangeBuilder
        .fanoutExchange("$prefix.notification.dlx")
        .configureAndBuild()

    private fun <T : Exchange> ExchangeBuilder.configureAndBuild(): T {
        durable(true)
        if (properties.rabbit.autoDelete) {
            autoDelete()
        }
        return build()
    }

    ////////////// QUEUES //////////////

    @Bean
    fun deadLetterQueue(): Queue = createQueue("$prefix.notification.dlq", false)

    @Bean
    fun emailingQueue(): Queue = createQueue("$prefix.notification.email", true)

    @Bean
    fun smsQueue(): Queue = createQueue("$prefix.notification.sms", true)

    @Bean
    fun webHookQueue(): Queue = createQueue("$prefix.notification.webhook", true)

    @Bean
    fun webSocketQueue(): Queue = createQueue("$prefix.notification.websocket", true)

    private fun createQueue(
        name: String,
        useDeadLetterQueue: Boolean
    ): Queue {
        val queueBuilder: QueueBuilder = QueueBuilder.durable(name)
        if (properties.rabbit.autoDelete) {
            queueBuilder.autoDelete()
        }
        if (useDeadLetterQueue) {
            queueBuilder.deadLetterExchange(deadLetterExchange().name)
        }
        queueBuilder.lazy()
        return queueBuilder.build()
    }

    ////////////// BINDINGS //////////////

    @Bean
    fun deadLetterBinding(): Binding = BindingBuilder
        .bind(deadLetterQueue())
        .to(deadLetterExchange())

    @Bean
    fun emailingBinding(): Binding = BindingBuilder
        .bind(emailingQueue())
        .to(notificationExchange())
        .with(notificationRoutingKey(NotificationInfo.Type.EMAIL.toRabbitToken()))

    @Bean
    fun smsBinding(): Binding = BindingBuilder
        .bind(smsQueue())
        .to(notificationExchange())
        .with(notificationRoutingKey(NotificationInfo.Type.SMS.toRabbitToken()))

    @Bean
    fun webHookBinding(): Binding = BindingBuilder
        .bind(webHookQueue())
        .to(notificationExchange())
        .with(notificationRoutingKey(NotificationInfo.Type.WEBHOOK.toRabbitToken()))

    @Bean
    fun webSocketTopicBinding(): Binding = BindingBuilder
        .bind(webSocketQueue())
        .to(notificationExchange())
        .with(notificationRoutingKey(NotificationInfo.Type.WEBSOCKET.toRabbitToken()))

    ////////////// TEMPLATES //////////////

    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory, objectMapper: ObjectMapper): RabbitTemplate {
        val callback = RabbitCallback()

        val rabbitTemplate = RabbitTemplate(connectionFactory)
        rabbitTemplate.messageConverter = jacksonMessageConverter(objectMapper)
        rabbitTemplate.setMandatory(true)
        rabbitTemplate.setReturnsCallback(callback)
        rabbitTemplate.setConfirmCallback(callback)
        return rabbitTemplate
    }

    @Bean
    fun jacksonMessageConverter(objectMapper: ObjectMapper) = Jackson2JsonMessageConverter(objectMapper)

}