package io.sirius.notification.providers

import io.sirius.notification.providers.emailing.EmailingProvider
import io.sirius.notification.providers.sms.SmsProvider

/**
 * Contains configuration for all providers
 */
data class ProvidersDescriptor(
    val emailing: List<ProviderConfig<EmailingProvider>>,
    val sms: List<ProviderConfig<SmsProvider>>
)