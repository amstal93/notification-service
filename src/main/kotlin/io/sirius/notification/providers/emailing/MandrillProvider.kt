package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.NotificationSendingException
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.json
import io.sirius.notification.util.HttpClientExt.jsonList
import io.sirius.notification.util.HttpClientExt.post
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.toBase64
import io.sirius.notification.util.toProviderMetadata
import org.apache.http.impl.client.CloseableHttpClient
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI


class MandrillProvider(
    override val provider: String,
    override val id: String = provider,
    private val httpClient: CloseableHttpClient,
    private val objectMapper: ObjectMapper,
    private val serverUri: URI = URI.create("https://mandrillapp.com/api/1.0"),
    private val apiKey: String,
    private val subAccount: String?
) : EmailingProvider {

    override fun sendEmail(emailInfo: EmailInfo) {
        val uriBuilder = UriComponentsBuilder.fromUri(serverUri)
            .pathSegment("messages", "send.json")

        httpClient.post(
            endpoint = uriBuilder.build().toUri(),
            entity = emailInfo.toMandrillPayload().toHttpEntity(objectMapper)
        ) { response ->
            response.ifError {
                val error = response.json<MandrillError>(objectMapper)
                throw HttpException("Mandrill error: ${error.message} (${error.code})", it)
            }

            val recipientsResponses = response.jsonList<MandrillRecipientResponse>(objectMapper)
            val failedRecipients = recipientsResponses.filter { it.isError() }
            if (failedRecipients.isNotEmpty()) {
                val reasons = failedRecipients.map { it.reject_reason }.toSet()
                throw NotificationSendingException("Mandrill errors: $reasons")
            }
        }
    }

    private fun EmailInfo.toMandrillPayload() = MandrillPayload(
        apiKey,
        MandrillMessage(
            subaccount = subAccount,
            html = htmlText,
            text = plainText,
            subject = subject,
            from_email = from.email,
            from_name = from.personal,
            to = this.toMandrillRecipients(),
            attachments = attachments.map {
                MandrillMessage.Attachment(
                    it.contentType,
                    it.filename,
                    it.data.toBase64()
                )
            },
            metadata = toProviderMetadata(this@MandrillProvider)
        )
    )

    private fun EmailInfo.toMandrillRecipients(): List<MandrillMessage.Recipient> {
        val recipients = mutableListOf<MandrillMessage.Recipient>()
        to.forEach { recipients.add(MandrillMessage.Recipient(it.email, it.personal, "to")) }
        cc.forEach { recipients.add(MandrillMessage.Recipient(it.email, it.personal, "cc")) }
        bcc.forEach { recipients.add(MandrillMessage.Recipient(it.email, it.personal, "bcc")) }
        return recipients.toList()
    }

    internal data class MandrillPayload(
        val key: String, // API key,
        val message: MandrillMessage
    )

    internal data class MandrillMessage(
        val html: String?,
        val text: String?,
        val subject: String,
        val from_email: String,
        val from_name: String?,
        val to: List<Recipient>,
        val attachments: List<Attachment>,
        val subaccount: String?,
        val metadata: ProviderMetadata
    ) {

        data class Recipient(
            val email: String,
            val name: String?,
            val type: String
        )

        data class Attachment(
            val type: String,
            val name: String,
            val content: String
        )
    }

    internal data class MandrillRecipientResponse(
        val email: String,
        val status: Status,
        val reject_reason: String?,
        val _id: String,
    ) {

        fun isError(): Boolean = (status == Status.invalid || status == Status.rejected)

        enum class Status { sent, queued, scheduled, rejected, invalid }
    }

    internal data class MandrillError(
        val status: String,
        val code: Int,
        val name: String?,
        val message: String
    )
}

data class MandrillProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val apiKey: String,
    val subAccount: String?,
) : ProviderConfig<MandrillProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ) = MandrillProvider(
        id = id,
        provider = provider,
        httpClient = httpClient,
        objectMapper = objectMapper,
        apiKey = apiKey,
        subAccount = subAccount
    )

}