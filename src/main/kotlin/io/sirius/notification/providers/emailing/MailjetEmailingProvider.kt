package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.*
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.json
import io.sirius.notification.util.HttpClientExt.post
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import org.apache.http.impl.client.CloseableHttpClient
import java.net.URI

/**
 * https://dev.mailjet.com/email/guides/getting-started/#send-your-first-email
 */
class MailjetEmailingProvider(
    override val provider: String,
    override val id: String = provider,
    private val httpClient: CloseableHttpClient,
    private val objectMapper: ObjectMapper,
    private val credentials: PasswordCredentials,
    private val serverUri: URI = URI.create("https://api.mailjet.com/v3.1/send")
) : EmailingProvider {

    private val logger = logger()

    override fun sendEmail(emailInfo: EmailInfo) {
        httpClient.post(
            endpoint = serverUri,
            entity = MailjetMessages(listOf(emailInfo.toMailjetMessage())).toHttpEntity(objectMapper),
            credentials = credentials
        ) { response ->
            response.ifError { status ->
                val error = response.json<MailjetError>(objectMapper)
                throw HttpException(error.ErrorMessage, status)
            }
            logger.info("Mail sent to mailjet")
        }
    }

    private fun EmailInfo.toMailjetMessage() = MailjetMessage(
        From = from.toMailjetAddress(),
        To = to.map { it.toMailjetAddress() },
        Cc = cc.map { it.toMailjetAddress() },
        Bcc = bcc.map { it.toMailjetAddress() },
        Subject = subject,
        TextPart = plainText,
        HTMLPart = htmlText,
        Attachments = attachments.map { it.toMailjetAttachment() },
        EventPayload = toProviderMetadata(this@MailjetEmailingProvider).serialize()
    )

    private fun EmailInfo.EmailAddress.toMailjetAddress() = MailjetMessage.Address(email, personal)

    private fun ProviderMetadata.serialize(): String = objectMapper.writeValueAsString(this)

    private fun EmailInfo.Attachment.toMailjetAttachment() = MailjetMessage.Attachment(
        ContentType = contentType,
        Filename = filename,
        Base64Content = data.toBase64()
    )

    internal data class MailjetMessages(@JvmField val Messages: List<MailjetMessage>)

    internal data class MailjetMessage(
        @JvmField val From: Address,
        @JvmField val To: List<Address>,
        @JvmField val Cc: List<Address> = emptyList(),
        @JvmField val Bcc: List<Address> = emptyList(),
        @JvmField val Subject: String,
        @JvmField val TextPart: String?,
        @JvmField val HTMLPart: String?,
        @JvmField val Attachments: List<Attachment> = emptyList(),
        @JvmField val EventPayload: String
    ) {

        internal data class Address(
            @JvmField val Email: String,
            @JvmField val Name: String? = null
        )

        internal data class Attachment(
            @JvmField val ContentType: String,
            @JvmField val Filename: String,
            @JvmField val Base64Content: String
        )
    }

    internal data class MailjetError(
        @JvmField val ErrorMessage: String,
        @JvmField val ErrorRelatedTo: List<String>?
    )
}

data class MailjetProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val apiKey: String,
    val secretKey: String
) : ProviderConfig<MailjetEmailingProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ) = MailjetEmailingProvider(
        id = id,
        provider = provider,
        httpClient = httpClient,
        objectMapper = objectMapper,
        credentials = PasswordCredentials(apiKey, secretKey)
    )
}