package io.sirius.notification.providers.emailing

import io.sirius.notification.model.EmailInfo
import io.sirius.notification.providers.NotificationProvider

interface EmailingProvider : NotificationProvider {

    fun sendEmail(emailInfo: EmailInfo)
}