package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.BearerCredentials
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.json
import io.sirius.notification.util.HttpClientExt.post
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.toBase64
import io.sirius.notification.util.toProviderMetadata
import org.apache.http.impl.client.CloseableHttpClient
import java.net.URI

class SendGridProvider(
    override val provider: String,
    override val id: String = provider,
    private val httpClient: CloseableHttpClient,
    private val credentials: BearerCredentials,
    private val serverUri: URI = URI.create("https://api.sendgrid.com/v3/mail/send"),
    private val objectMapper: ObjectMapper
) : EmailingProvider {

    override fun sendEmail(emailInfo: EmailInfo) {
        httpClient.post(
            endpoint = serverUri,
            entity = emailInfo.toSendGridMessage().toHttpEntity(objectMapper),
            credentials = credentials
        ) { response ->
            response.ifError {
                val errors = response.json<SendGridErrors>(objectMapper)
                throw HttpException(errors.errors.joinToString(separator = " - "), it)
            }
        }
    }

    private fun EmailInfo.toSendGridMessage() = SendGridMessage(
        personalizations = listOf(
            SendGridMessage.Personalization(
                to = to.map { it.toSendGridAddress() },
                cc = cc.map { it.toSendGridAddress() }.takeIf { it.isNotEmpty() },
                bcc = bcc.map { it.toSendGridAddress() }.takeIf { it.isNotEmpty() },
                subject = subject,
                custom_args = toProviderMetadata(this@SendGridProvider)
            )
        ),
        from = from.toSendGridAddress(),
        content = this.toContents(),
        attachments = attachments.map { it.toSendGridAttachment() }.takeIf { it.isNotEmpty() }
    )

    private fun EmailInfo.EmailAddress.toSendGridAddress() = SendGridMessage.Address(email, personal)

    private fun EmailInfo.Attachment.toSendGridAttachment() = SendGridMessage.Attachment(
        content = data.toBase64(),
        type = contentType,
        filename = filename
    )

    private fun EmailInfo.toContents(): List<SendGridMessage.Content> {
        val contents = mutableListOf<SendGridMessage.Content>()
        if (plainText?.isNotBlank() == true) {
            contents.add(SendGridMessage.Content("text/plain", plainText))
        }
        if (htmlText?.isNotBlank() == true) {
            contents.add(SendGridMessage.Content("text/html", htmlText))
        }
        return contents
    }

    internal data class SendGridMessage(
        val personalizations: List<Personalization>,
        val from: Address,
        val content: List<Content>,
        val attachments: List<Attachment>?
    ) {

        data class Personalization(
            val to: List<Address>,
            val cc: List<Address>?,
            val bcc: List<Address>?,
            val subject: String,
            val custom_args: ProviderMetadata
        )

        data class Address(val email: String, val name: String? = null)

        data class Content(val type: String, val value: String)

        data class Attachment(val content: String, val type: String, val filename: String)
    }

    internal data class SendGridErrors(val errors: List<Error>) {

        data class Error(val message: String, val field: String?)
    }
}

data class SendGridProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val apiKey: String
) : ProviderConfig<SendGridProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ) = SendGridProvider(
        id = id,
        provider = provider,
        httpClient = httpClient,
        objectMapper = objectMapper,
        credentials = BearerCredentials(apiKey)
    )
}