package io.sirius.notification.providers

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.providers.emailing.MailjetProviderConfig
import io.sirius.notification.providers.emailing.MandrillProviderConfig
import io.sirius.notification.providers.emailing.SendGridProviderConfig
import io.sirius.notification.providers.emailing.SmtpProviderConfig
import io.sirius.notification.providers.sms.AllMySmsProviderConfig
import io.sirius.notification.providers.sms.DummySmsProviderConfig
import io.sirius.notification.providers.sms.EsendexProviderConfig
import io.sirius.notification.providers.sms.MailjetSmsProviderConfig
import org.apache.http.impl.client.CloseableHttpClient

/**
 * This class contains configuration for a single provider.
 */
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    visible = true,
    property = "provider"
)
@JsonSubTypes(
    // Email
    JsonSubTypes.Type(value = SmtpProviderConfig::class, name = "smtp"),
    JsonSubTypes.Type(value = MailjetProviderConfig::class, name = "mailjet"),
    JsonSubTypes.Type(value = SendGridProviderConfig::class, name = "sendGrid"),
    JsonSubTypes.Type(value = MandrillProviderConfig::class, name = "mandrill"),
    // SMS
    JsonSubTypes.Type(value = EsendexProviderConfig::class, name = "esendex"),
    JsonSubTypes.Type(value = AllMySmsProviderConfig::class, name = "allMySms"),
    JsonSubTypes.Type(value = MailjetSmsProviderConfig::class, name = "mailjetSms"),
    JsonSubTypes.Type(value = DummySmsProviderConfig::class, name = "dummySms")
)
interface ProviderConfig<T : NotificationProvider> {

    val provider: String
    val id: String

    /**
     * Create a fresh [NotificationProvider] configured with its configuration
     *
     * @param httpClient A HttpClient if needed
     * @param objectMapper A Jackson mapper if needed
     */
    fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ): T

}