package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.AccessRestrictedException
import io.sirius.notification.EntityTooLargeException
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.HttpClientExt.httpStatus
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.json
import io.sirius.notification.util.HttpClientExt.post
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.PasswordCredentials
import io.sirius.notification.util.logger
import org.apache.http.HttpHeaders
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.message.BasicHeader
import org.springframework.http.HttpStatus
import java.net.URI

/**
 * https://developers.esendex.com/api-reference#restapi
 */
class EsendexProvider(
    override val provider: String,
    override val id: String = provider,
    private val httpClient: CloseableHttpClient,
    private val objectMapper: ObjectMapper,
    private val serverUri: URI = URI.create("https://api.esendex.com/v1.0/messagedispatcher"),
    private val credentials: PasswordCredentials,
    private val account: String
) : SmsProvider {

    private val logger = logger()

    // Characters counted as two chars in message
    private val dualChar: CharArray = "^{},[~]|€".toCharArray()

    override fun sendSms(smsInfo: SmsInfo): Int {
        val smsCount = getSmsCount(smsInfo.message)

        return httpClient.post(
            endpoint = serverUri,
            entity = smsInfo.toRequest().toHttpEntity(objectMapper),
            credentials = credentials,
            headers = listOf(BasicHeader(HttpHeaders.ACCEPT, "application/json"))
        ) { response ->
            if (response.httpStatus == HttpStatus.FORBIDDEN) {
                throw AccessRestrictedException("Access forbidden on Esendex provider")
            }

            val result = response.json<EsendexResult>(objectMapper)
            logger.trace("Esendex result: {}", result)

            response.ifError {
                val message = result.errors?.joinToString { err -> err.description }
                throw HttpException("Esendex error ($message)", it)
            }
            logger.info("Message sent with Esendex with batchId {}", result.batch!!.batchid)
            smsCount
        }
    }

    internal fun getSmsCount(message: String): Int = when (getSmsCharsCount(message)) {
        in 0..160 -> 1
        in 161..306 -> 2
        in 307..460 -> 3
        in 461..612 -> 4
        else -> throw EntityTooLargeException("SMS is too large")
    }

    internal fun getSmsCharsCount(message: String): Int {
        var charsCount = 0
        message.forEach { charsCount += if (dualChar.contains(it)) 2 else 1 }
        return charsCount
    }

    private fun SmsInfo.toRequest() = EsendexRequest(
        account,
        listOf(EsendexRequest.Message(from, phoneNumber, message))
    )

    internal data class EsendexRequest(
        val accountreference: String,
        val messages: List<Message>
    ) {

        data class Message(
            val from: String?, // Only ASCII allowed
            val to: String,
            val body: String // All chars allowed
        )
    }

    internal data class EsendexResult(
        val batch: Batch? = null,
        val errors: List<Error>? = null
    ) {

        data class Batch(
            val batchid: String,
            val messageheaders: List<MessageHeader>
        ) {
            data class MessageHeader(val id: String, val uri: URI)
        }

        data class Error(val code: String, val description: String)
    }

}

data class EsendexProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val account: String,
    val login: String,
    val password: String
) : ProviderConfig<EsendexProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ) = EsendexProvider(
        id = id,
        provider = provider,
        httpClient = httpClient,
        objectMapper = objectMapper,
        account = account,
        credentials = PasswordCredentials(login, password)
    )
}