package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.BearerCredentials
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.json
import io.sirius.notification.util.HttpClientExt.post
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.logger
import org.apache.http.impl.client.CloseableHttpClient
import java.net.URI


/**
 * https://dev.mailjet.com/sms/reference/send-message/
 */
class MailjetSmsProvider(
    override val provider: String,
    override val id: String = provider,
    private val httpClient: CloseableHttpClient,
    private val objectMapper: ObjectMapper,
    private val credentials: BearerCredentials,
    private val serverUri: URI = URI.create("https://api.mailjet.com/v4/REST/sms-send")
) : SmsProvider {

    private val logger = logger()

    override fun sendSms(smsInfo: SmsInfo): Int {
        return httpClient.post(
            endpoint = serverUri,
            entity = smsInfo.toMailjetSms().toHttpEntity(objectMapper),
            credentials = credentials
        ) { response ->
            response.ifError { throw HttpException("Mailjet server respond HTTP/${it.value()}", it) }

            val result = response.json<MailjetResult>(objectMapper)
            logger.info("SMS sent to mailjet with identifier {}", result.MessageId)
            result.SmsCount
        }
    }

    private fun SmsInfo.toMailjetSms() = MailjetSms(from, phoneNumber, message)

    internal data class MailjetSms(
        @JvmField val From: String,
        @JvmField val To: String,
        @JvmField val Text: String
    )

    internal data class MailjetResult(
        @JvmField val SmsCount: Int,
        @JvmField val MessageId: String
    )

}

data class MailjetSmsProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val token: String
) : ProviderConfig<MailjetSmsProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ) = MailjetSmsProvider(
        id = id,
        provider = provider,
        httpClient = httpClient,
        objectMapper = objectMapper,
        credentials = BearerCredentials(token)
    )

}