package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.json
import io.sirius.notification.util.HttpClientExt.post
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.PasswordCredentials
import io.sirius.notification.util.logger
import org.apache.http.impl.client.CloseableHttpClient
import java.net.URI

/**
 * https://doc.allmysms.com/api/fr/
 */
class AllMySmsProvider(
    override val provider: String,
    override val id: String = provider,
    private val httpClient: CloseableHttpClient,
    private val objectMapper: ObjectMapper,
    private val serverUri: URI = URI.create("https://api.allmysms.com/sms/send"),
    private val credentials: PasswordCredentials
) : SmsProvider {

    private val logger = logger()

    override fun sendSms(smsInfo: SmsInfo): Int {
        return httpClient.post(
            endpoint = serverUri,
            entity = smsInfo.toAllMySmsMessage().toHttpEntity(objectMapper),
            credentials = credentials
        ) { response ->
            response.ifError {
                val error = response.json<AllMySmsError>(objectMapper)
                throw HttpException(error.description, it)
            }

            val result = response.json<AllMySmsResult>(objectMapper)
            logger.info("SMS sent with AllMySms with identifier {}", result.smsId)
            result.nbSms
        }
    }


    private fun SmsInfo.toAllMySmsMessage() = AllMySmsMessage(phoneNumber, message, from)

    internal data class AllMySmsMessage(
        val to: String,
        val text: String,
        val from: String?
    )

    internal data class AllMySmsResult(
        val nbSms: Int,
        val smsId: String
    )

    internal data class AllMySmsError(val description: String, val code: String)

}

data class AllMySmsProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val login: String,
    val apiKey: String
) : ProviderConfig<AllMySmsProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ) = AllMySmsProvider(
        id = id,
        provider = provider,
        httpClient = httpClient,
        objectMapper = objectMapper,
        credentials = PasswordCredentials(login, apiKey)
    )

}