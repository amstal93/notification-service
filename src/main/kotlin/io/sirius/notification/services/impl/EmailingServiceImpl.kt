package io.sirius.notification.services.impl

import io.sirius.notification.config.RabbitConfig.Companion.HEADER_X_DEATH
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.providers.emailing.EmailingProvider
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.services.NotificationService
import io.sirius.notification.services.ProviderService
import io.sirius.notification.util.getTryIndex
import io.sirius.notification.util.logger
import io.sirius.notification.util.mdc
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Service

@Service
class EmailingServiceImpl(
    private val providerService: ProviderService<EmailingProvider>,
    private val exchangeService: ExchangeService
) : NotificationService<EmailInfo> {

    private val logger = logger()

    @RabbitListener(
        queues = ["#{@emailingQueue.name}"],
        id = "notification-email"
    )
    override fun processNotification(
        notification: EmailInfo,
        @Header(HEADER_X_DEATH) xDeathHeader: List<Map<String, *>>?
    ) {
        mdc {
            notification.mdc()
            val retryIndex = xDeathHeader.getTryIndex()
            val provider = providerService.getProvider(retryIndex, notification.providers)
            provider.sendEmail(notification)
            logger.info("Email sent")

            exchangeService.sendSuccessCallback(
                notification = notification,
                provider = provider,
                tryIndex = retryIndex
            )
        }
    }
}