package io.sirius.notification.services.impl

import io.sirius.notification.config.RabbitConfig
import io.sirius.notification.model.*
import io.sirius.notification.providers.NotificationProvider
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.util.logger
import io.sirius.notification.util.mdc
import io.sirius.notification.util.toRabbitToken
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service

@Service
class ExchangeServiceImpl(
    private val notificationExchangeName: String,
    private val notificationCallbackExchangeName: String,
    private val rabbitTemplate: RabbitTemplate
) : ExchangeService {

    private val logger = logger()

    override fun submitNotification(notification: NotificationInfo) {
        mdc {
            notification.mdc()
            rabbitTemplate.convertAndSend(
                notificationExchangeName,
                RabbitConfig.notificationRoutingKey(notification.type.toRabbitToken()),
                notification
            )
            logger.info(
                "Notification {} pushed to Rabbit with identifier {}",
                notification.type, notification.id
            )
        }
    }

    override fun sendSuccessCallback(
        notification: NotificationInfo,
        provider: NotificationProvider?,
        tryIndex: Int,
        count: Int
    ) {
        val callback = NotificationSuccessCallback(
            notification = notification.toNotificationCallbackInfo(),
            metadata = notification.metadata,
            provider = provider?.toProviderCallbackInfo(),
            tryIndex = tryIndex,
            counter = count
        )
        rabbitTemplate.convertAndSend(
            notificationCallbackExchangeName,
            RabbitConfig.callbackRoutingKey(
                notificationType = notification.type.toRabbitToken()
            ),
            callback
        )
        logger.info("Notification success callback sent: {}", callback)
    }

    override fun sendErrorCallback(
        notification: NotificationInfo,
        retriesCount: Int
    ) {
        val callback = NotificationErrorCallback(
            notification = notification.toNotificationCallbackInfo(),
            metadata = notification.metadata,
            tries = retriesCount - 1
        )
        rabbitTemplate.convertAndSend(
            notificationCallbackExchangeName,
            RabbitConfig.errorCallbackRoutingKey(
                notificationType = notification.type.toRabbitToken()
            ),
            callback
        )
        logger.info("Notification error callback sent: {}", callback)
    }

    override fun sendTrackingEvent(event: TrackingEvent) {
        mdc {
            event.mdc()
            rabbitTemplate.convertAndSend(
                notificationCallbackExchangeName,
                RabbitConfig.trackingRoutingKey(
                    notificationType = event.notification.type.toRabbitToken(),
                    trackingType = event.eventType.toRabbitToken()
                ),
                event
            )
            logger.info("Tracking event sent: {}", event)
        }
    }

    private fun NotificationProvider.toProviderCallbackInfo() =
        NotificationCallback.ProviderCallbackInfo(id = id, type = provider)

    private fun NotificationInfo.toNotificationCallbackInfo() =
        NotificationCallback.NotificationCallbackInfo(id = id, type = type)
}