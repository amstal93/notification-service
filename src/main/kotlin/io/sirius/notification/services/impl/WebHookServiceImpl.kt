package io.sirius.notification.services.impl

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.config.RabbitConfig.Companion.HEADER_X_DEATH
import io.sirius.notification.model.WebHookInfo
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.services.NotificationService
import io.sirius.notification.util.HttpClientExt.ifError
import io.sirius.notification.util.HttpClientExt.toHttpEntity
import io.sirius.notification.util.HttpException
import io.sirius.notification.util.getTryIndex
import io.sirius.notification.util.logger
import io.sirius.notification.util.mdc
import org.apache.http.HttpEntityEnclosingRequest
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.util.EntityUtils
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets

@Service
class WebHookServiceImpl(
    private val httpClient: CloseableHttpClient,
    private val exchangeService: ExchangeService,
    private val objectMapper: ObjectMapper
) : NotificationService<WebHookInfo<*>> {

    private val logger = logger()

    @RabbitListener(
        queues = ["#{@webHookQueue.name}"],
        id = "notification-webhook"
    )
    override fun processNotification(
        notification: WebHookInfo<*>,
        @Header(HEADER_X_DEATH) xDeathHeader: List<Map<String, *>>?
    ) {
        mdc {
            notification.mdc()
            val retryIndex = xDeathHeader.getTryIndex()
            val request: HttpUriRequest = when (notification.method) {
                WebHookInfo.Method.GET -> HttpGet(notification.remoteUri)
                WebHookInfo.Method.POST -> HttpPost(notification.remoteUri)
                WebHookInfo.Method.PUT -> HttpPut(notification.remoteUri)
            }

            // Set payload if available
            val body: Any? = notification.body
            if (request is HttpEntityEnclosingRequest && body != null) {
                if (body is String) {
                    request.entity = StringEntity(body, StandardCharsets.UTF_8)
                } else {
                    request.entity = body.toHttpEntity(objectMapper)
                }
            }

            // Add headers
            notification.headers.forEach { request.addHeader(it.name, it.value) }

            httpClient.execute(request).use { response ->
                response.ifError { throw HttpException("Webhook failed with status HTTP/${it.value()}", it) }
                EntityUtils.consume(response.entity)
            }
            logger.info("Webhook send successfully to {} {}", notification.method, notification.remoteUri)

            exchangeService.sendSuccessCallback(
                notification = notification,
                tryIndex = retryIndex,
                provider = null
            )
        }
    }
}