package io.sirius.notification.services.impl

import io.sirius.notification.providers.NotificationProvider
import io.sirius.notification.services.ProviderService
import java.util.*
import kotlin.collections.ArrayList

class ProviderServiceImpl<T : NotificationProvider>(
    override val providers: List<T>
) : ProviderService<T> {

    override fun getProvider(
        index: Int,
        requestedProvidersIds: Set<String>
    ): T {
        val copiedList: MutableList<T> = if (requestedProvidersIds.isNotEmpty()) {
            requestedProvidersIds.validate()
            providers.filter { requestedProvidersIds.contains(it.id) }.toMutableList()
        } else {
            ArrayList(providers) // Copy
        }
        require(copiedList.isNotEmpty()) { "No provider available" }

        // Round Robin ... more or less!!!
        Collections.rotate(copiedList, -index)
        return copiedList.first()
    }

    /** Check if all provider identifiers exists */
    private fun Set<String>.validate() {
        val availableProviders = providers.map { it.id }
        this.forEach { require(availableProviders.contains(it)) { "Unknown provider $it" } }
    }
}