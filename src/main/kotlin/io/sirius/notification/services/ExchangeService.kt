package io.sirius.notification.services

import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.model.TrackingEvent
import io.sirius.notification.providers.NotificationProvider

interface ExchangeService {

    /**
     * Submit a notification (this does not send the notification)
     *
     * @param notification The notification to submit in the system
     */
    fun submitNotification(notification: NotificationInfo)

    fun sendSuccessCallback(
        notification: NotificationInfo,
        provider: NotificationProvider?,
        tryIndex: Int,
        count: Int = 1
    )

    fun sendErrorCallback(
        notification: NotificationInfo,
        retriesCount: Int
    )

    fun sendTrackingEvent(event: TrackingEvent)
}