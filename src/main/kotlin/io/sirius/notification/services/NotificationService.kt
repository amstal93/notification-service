package io.sirius.notification.services

import io.sirius.notification.config.RabbitConfig.Companion.HEADER_X_DEATH
import io.sirius.notification.model.NotificationInfo
import org.springframework.messaging.handler.annotation.Header

interface NotificationService<T : NotificationInfo> {

    /**
     * Called when a new notification is available to be sent
     *
     * @param notification The notification to send
     * @param xDeathHeader The x-death header used when retry
     */
    fun processNotification(
        notification: T,
        @Header(HEADER_X_DEATH) xDeathHeader: List<Map<String, *>>? = null
    )

}