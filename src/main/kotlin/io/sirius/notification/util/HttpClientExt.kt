package io.sirius.notification.util

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.Header
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.AbstractHttpEntity
import org.apache.http.entity.ContentType
import org.apache.http.impl.client.CloseableHttpClient
import org.springframework.http.HttpStatus
import java.io.*
import java.net.URI
import java.nio.charset.Charset

object HttpClientExt {

    fun <T> CloseableHttpClient.post(
        endpoint: URI,
        entity: HttpEntity? = null,
        credentials: Credentials? = null,
        headers: List<Header> = emptyList(),
        responseMapper: (HttpResponse) -> T
    ): T {
        val request = HttpPost(endpoint)
        request.entity = entity
        headers.forEach { request.addHeader(it) }
        credentials?.let { request.addHeader(it.toHeader()) }

        return execute(request).use { responseMapper(it) }
    }

    fun <T> CloseableHttpClient.get(
        endpoint: URI,
        headers: List<Header> = emptyList(),
        responseMapper: (HttpResponse) -> T
    ): T {
        val request = HttpGet(endpoint)
        headers.forEach { request.addHeader(it) }

        return execute(request).use { responseMapper(it) }
    }

    inline fun <reified T> HttpResponse.json(mapper: ObjectMapper): T {
        ensureJsonResponse()
        return entity.reader.use { mapper.readValue(it, T::class.java) }
    }

    inline fun <reified T> HttpResponse.jsonList(mapper: ObjectMapper): List<T> {
        ensureJsonResponse()
        return entity.reader.use { mapper.readList(it) }
    }

    val HttpEntity.reader: Reader
        get() {
            val charset: Charset = ContentType.get(this)?.charset ?: Charsets.UTF_8
            return InputStreamReader(content, charset)
        }

    fun HttpResponse.ensureJsonResponse() {
        val contentType = ContentType.get(entity)
        if (contentType?.mimeType != ContentType.APPLICATION_JSON.mimeType) {
            throw UnsupportedOperationException("Only JSON payload can be read, but found $contentType")
        }
    }

    fun Any.toHttpEntity(objectMapper: ObjectMapper): HttpEntity = JacksonEntity(this, objectMapper)

    val HttpResponse.httpStatus: HttpStatus
        get() = HttpStatus.valueOf(statusLine.statusCode)

    fun HttpResponse.ifError(errorAction: (HttpStatus) -> Unit) {
        if (httpStatus.isError) {
            errorAction(httpStatus)
        }
    }

    /** HttpEntity to serialize object in JSON using Jackson */
    private class JacksonEntity(
        private val obj: Any,
        private val objectMapper: ObjectMapper
    ) : AbstractHttpEntity() {

        init {
            setContentType("application/json")
            setContentEncoding("UTF-8")
        }

        override fun isRepeatable(): Boolean = true

        override fun getContentLength(): Long {
            val counterStream = CounterOutputStream()
            writeTo(counterStream)
            return counterStream.count
        }

        override fun getContent(): InputStream {
            val data = objectMapper.writeValueAsBytes(obj)
            return ByteArrayInputStream(data)
        }

        override fun writeTo(outStream: OutputStream) {
            objectMapper.writeValue(outStream, obj)
        }

        override fun isStreaming(): Boolean = false

        /** Stream only counting bytes */
        private class CounterOutputStream(stream: OutputStream = NullOutputStream()) : FilterOutputStream(stream) {

            var count: Long = 0

            override fun write(b: ByteArray, off: Int, len: Int) {
                super.write(b, off, len)
                count += len
            }
        }

        /** Stream doing nothing! All data are lost. */
        private class NullOutputStream : OutputStream() {

            override fun write(b: Int) {
                // Nothing to do
            }

            override fun write(b: ByteArray, off: Int, len: Int) {
                // Nothing to do
            }
        }
    }
}