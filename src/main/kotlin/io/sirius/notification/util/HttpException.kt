package io.sirius.notification.util

import org.springframework.http.HttpStatus

class HttpException(msg: String, val status: HttpStatus) : Exception(msg)