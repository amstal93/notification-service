package io.sirius.notification.util

import io.sirius.notification.model.EmailInfo
import javax.mail.Address
import javax.mail.Message
import javax.mail.Multipart
import javax.mail.Part
import javax.mail.internet.ContentType
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimePart

object MimeMessageExt {

    fun MimeMessage.getFromAddress(): EmailInfo.EmailAddress = from[0].toEmailAddress()

    fun MimeMessage.getTo(): List<EmailInfo.EmailAddress> =
        getRecipients(Message.RecipientType.TO)?.map { it.toEmailAddress() } ?: emptyList()

    fun MimeMessage.getCc(): List<EmailInfo.EmailAddress> =
        getRecipients(Message.RecipientType.CC)?.map { it.toEmailAddress() } ?: emptyList()

    fun MimeMessage.getBcc(): List<EmailInfo.EmailAddress> =
        getRecipients(Message.RecipientType.BCC)?.map { it.toEmailAddress() } ?: emptyList()

    private fun Address.toEmailAddress(): EmailInfo.EmailAddress {
        require(this is InternetAddress)
        return EmailInfo.EmailAddress(address, personal)
    }

    fun MimeMessage.isMultipart(): Boolean = content is Multipart

    fun MimeMessage.getPlaintText(): String? = searchText(this, "text/plain")?.trim()

    fun MimeMessage.getHtmlText(): String? = searchText(this, "text/html")?.trim()

    private fun searchText(part: MimePart, expectedContentType: String): String? {
        val ct = ContentType(part.contentType)
        val content = part.content

        if (content is String && ct.baseType == expectedContentType) {
            return content

        } else if (content is Multipart) {
            for (i in 1..content.count) {
                val result = searchText(content.getBodyPart(i - 1) as MimePart, expectedContentType)
                if (result != null) {
                    return result
                }
            }
        }
        return null
    }

    fun MimeMessage.getAttachments(): List<EmailInfo.Attachment> {
        val attachments = mutableListOf<EmailInfo.Attachment>()
        searchAttachments(this, attachments)
        return attachments
    }

    private fun searchAttachments(part: MimePart, attachments: MutableList<EmailInfo.Attachment>) {
        if (part.disposition == Part.ATTACHMENT) {
            attachments.add(
                EmailInfo.Attachment(
                    part.fileName,
                    ContentType(part.contentType).baseType,
                    part.inputStream.use { it.readAllBytes() })
            )
        }

        val content = part.content
        if (content is Multipart) {
            for (i in 1..content.count) {
                searchAttachments(content.getBodyPart(i - 1) as MimePart, attachments)
            }
        }
    }
}