package io.sirius.notification.util

import org.springframework.amqp.core.ReturnedMessage
import org.springframework.amqp.rabbit.connection.CorrelationData
import org.springframework.amqp.rabbit.core.RabbitTemplate

class RabbitCallback : RabbitTemplate.ReturnsCallback, RabbitTemplate.ConfirmCallback {

    private val logger = logger()

    override fun returnedMessage(returned: ReturnedMessage) {
        logger.warn(
            "A message has been returned from exchange {}/{} with code {} and text {}",
            returned.exchange, returned.routingKey, returned.replyCode, returned.replyText
        )
    }

    override fun confirm(
        correlationData: CorrelationData?,
        ack: Boolean,
        cause: String?
    ) {
        if (!ack) {
            logger.error(
                "A message has not been confirmed {} ({})",
                correlationData?.returned, cause
            )
        }
    }

}