FROM openjdk:11-jre-slim

# Set time zone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Start server
COPY build/libs/notification-service-*.jar /app/notification.jar
WORKDIR "/app"
ENTRYPOINT ["java", "-jar", "/app/notification.jar"]

EXPOSE 8080
