# API

You can use the REST API or push directly in the RabbitMQ exchange `sirius.notification`. 
The same payload is used in both.

If you choose the RabbitMQ approach, you have to use the specific routing key (more details below).

APIs to submit notification use basic Authentication. The default login is `sirius` and password is `sirius`.
For security reason, you should change it in [configuration](configuration.md#notification-configuration).

## Send an email

POST /api/email   
Content-Type: application/json

OR

Routing key: `notification.email`

```json
{
  "id": "123456",
  "from": {
    "email": "from@acme.com",
    "personal": "John Doe"
  },
  "subject": "your subject",
  "plainText": "Hello world!",
  "htmlText": "<html>Hello world!</html>",
  "to": [{
    "email": "recipient@acme.com",
    "personal": "Jane Doe"
  }],
  "metadata": {
    "mykey": "myvalue"
  },
  "providers": ["smtp1", "smtp2"]
}
```

Here are the parameters:

| Key           | Type        | Required | Description                                                              |
|---------------|-------------|----------|--------------------------------------------------------------------------|
| id            | String      | no       | An identifier for this notification. If not present an UUID is generated |
| metadata      | Map         | no       | A map with key value. It will be present in [callback](rabbitmq.md#callback) |
| from          | Email       | yes      | The email to set as "from"                                               |
| to            | Email array | yes      | The recipients to set in the email                                       |
| cc            | Email array | no       | The recipients copy to set in the email                                  |
| bcc           | Email array | no       | The hidden recipients copy to set in the email                           |
| subject       | String      | yes      | The email subject                                                        |
| plainText     | String      | no       | The email plain text content                                             |
| htmlText      | String      | no       | The email HTML text content                                              |
| providers     | String array| no       | The providers identifiers to use. Must match the `id` in [descriptor](providers.md#descriptor). It is useful when a client only want to use a specific provider |

Both `plainText` and `htmlText` can be set in the same time. In this case the built email will contain a multipart
related content. The HTML part will be displayed first in mailer.

The email payload contains the following structure:

| Key           | Type        | Required | Description                                                              |
|---------------|-------------|----------|--------------------------------------------------------------------------|
| email         | String      | yes      | The email address (RFC 3696)                                             |
| personal      | String      | no       | The email personal name (such as the recipient name or company name)     |


## Send an SMS

POST /api/sms   
Content-Type: application/json

OR

Routing key: `notification.sms`

```json
{
  "id": "123456",
  "phoneNumber": "+33606060606",
  "message": "Hello world!",
  "from": "My corp",
  "metadata": {
    "mykey": "myvalue"
  },
  "providers": ["sms1", "sms2"]
}
```

Here are the parameters:

| Key           | Type        | Required | Description                                                              |
|---------------|-------------|----------|--------------------------------------------------------------------------|
| id            | String      | no       | An identifier for this notification. If not present an UUID is generated |
| metadata      | Map         | no       | A map with key value. It will be present in [callback](rabbitmq.md#callback) |
| from          | Email       | yes      | The SMS sender name                                                      |
| message       | String      | yes      | The SMS message                                                          |
| phoneNumber   | String      | yes      | The recipient phone number in international format (+33xxx)              |
| providers     | String array| no       | The providers identifiers to use. Must match the `id` in [descriptor](providers.md#descriptor). It is useful when a client only want to use a specific provider |


## Send a webhook

POST /api/webhook   
Content-Type: application/json

OR

Routing key: `notification.webhook`

```json
{
  "id": "123456",
  "remoteUri": "http://remote.com/callback",
  "method": "POST",
  "headers": [{
    "name": "x-callback",
    "value": "value"
  }],
  "body": {
    
  },
  "metadata": {
    "mykey": "myvalue"
  }
}
```

Here are the parameters:

| Key           | Type        | Required | Description                                                              |
|---------------|-------------|----------|--------------------------------------------------------------------------|
| id            | String      | no       | An identifier for this notification. If not present an UUID is generated |
| metadata      | Map         | no       | A map with key value. It will be present in [callback](rabbitmq.md#callback) |
| remoteUri     | URI         | yes      | The URI to notify in the webhook                                         |
| method        | String      | no       | The HTTP method (`POST`, `PUT` or `GET`)                                 |
| body          | JSON        | no       | The body to send in the webhook payload (only for POST and PUT request)  |
| headers       | Header array| no       | The headers to add in the webhook request                                |


## Send a websocket

POST /api/websocket   
Content-Type: application/json

OR

Routing key: `notification.websocket.topic`

```json
{
  "id": "123456",
  "topic": "room",
  "body": {
    "type": "test"
  },
  "metadata": {
    "mykey": "myvalue"
  }
}
```

Here are the parameters:

| Key           | Type        | Required | Description                                                              |
|---------------|-------------|----------|--------------------------------------------------------------------------|
| id            | String      | no       | An identifier for this notification. If not present an UUID is generated |
| metadata      | Map         | no       | A map with key value. It will be present in [callback](rabbitmq.md#callback) |
| topic         | String      | yes      | The topic name to push the body into                                     |
| body          | JSON        | no       | The body to send in the websocket payload                                |