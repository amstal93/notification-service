# Providers

[[_TOC_]]

## Descriptor

The descriptor contains information on each provider. This file is named by default `providers.yml` and must be
saved in the application directory.

The descriptor is a simple JSON file and each notification type got its own providers array. They must be ordered
in the JSON because they are used in the same order for the retry (first try with the first provider, second try 
with the second provider, etc).

Each provider configuration contains these properties:

* `provider` is the provider system name to use (ie. "smtp" or "mandrill"). Each value is defined below (in each provider paragraph)
* `id` is the provider instance identifier. For instance, you can configure multiple "smtp" providers (with different properties),
  and so, this "id" is to identify each instance. If this property is not set, the value will be the same defined in `provider`

The descriptor looks like this:

```yaml
emailing:
  - provider: smtp
    id: gmail
    host: smtp.gmail.com
    port: 567
    username: fabar
    password: pwd
  - provider: smtp
    id: hotmail
    host: smtp.hotmail.com
    port: 567
    username: fabar
    password: pwd
  - provider: mailjet
    apiKey: fabar
    secretKey: pwd
sms:
  - provider: esendex
    account: EX123456
    login: fabar
    password: pwd
  - provider: allMySms
    login: fabar
    apiKey: pwd
```

The webhook and websocket notifications do not use providers because it is useless (it is basic HTTP notifications).

**For websocket you have to configure a STOMP relay (see [configuration file](configuration.md))**

## Email providers

### SMTP

Provider identifier: `smtp`

Represents a simple SMTP server. The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| host         | String | yes      | The SMTP hostname                                      |
| port         | Int    | yes      | The SMTP port                                          |
| username     | String | no       | The SMTP username for authentication                   |
| password     | String | no       | The SMTP password for authentication                   |

```yaml
- provider: smtp
  id: gmail
  host: smtp.gmail.com
  port: '465'
  username: login
  password: pwd
```

### Mailjet email

Provider identifier: `mailjet`

Use the [mailjet](https://www.mailjet.com/) API to send emails. You need an account to use this provider.
The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| apiKey       | String | yes      | The credentials to access the API                      |
| secretKey    | String | yes      | The credentials to access the API                      |

```yaml
- provider: mailjet
  id: mailjet
  apiKey: 123456789
  secretKey: 123456789
```

#### Tracking

The URL in the server to receive the tracking events is `/tracking/email/mailjet`.

You have to configure the webhook at this URL: [https://app.mailjet.com/account/triggers](https://app.mailjet.com/account/triggers)

![tracking_mailjet](images/tracking_mailjet.png)

When receiving an event in this webhook, a message is sent in the [tracking callback exchange](rabbitmq.md#notification-tracking).


### SendGrid

Provider identifier: `sendGrid`

Use the [SendGrid](https://sendgrid.com/) API to send emails. You need an account to use this provider.
The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| apiKey       | String | yes      | The credentials to access the API                      |

```yaml
- provider: sendGrid
  id: sendGrid
  apiKey: 123456789
```

#### Tracking

The URL in the server to receive the tracking events is `/tracking/email/sendgrid`.

You have to configure the webhook at this URL: [https://app.sendgrid.com/settings/mail_settings](https://app.sendgrid.com/settings/mail_settings)
then click on "Event webhook" link.

![tracking_sendgrid](images/tracking_sendgrid.png)

When receiving an event in this webhook, a message is sent in the [tracking callback exchange](rabbitmq.md#notification-tracking).


### Mandrill

Provider identifier: `mandrill`

Use the [Mandrill](https://mailchimp.com/fr/features/transactional-email/) API to send emails. You need an account to use this provider.
The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| apiKey       | String | yes      | The credentials to access the API                      |
| subAccount   | String | no       | The sub-account identifier if needed                   |

```yaml
- provider: mandrill
  id: mandrill
  apiKey: 123456789
```

#### Tracking

The URL in the server to receive the tracking events is `/tracking/email/mandrill`.

You have to configure the webhook at this URL: [https://mandrillapp.com/settings/webhooks](https://mandrillapp.com/settings/webhooks).

![tracking_sendgrid](images/tracking_mandrill.png)

When receiving an event in this webhook, a message is sent in the [tracking callback exchange](rabbitmq.md#notification-tracking).


## SMS providers

### Esendex

Provider identifier: `esendex`

Use the [Esendex](https://www.esendex.co.uk/) API to send SMS. You need an account to use this provider.
The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| account      | String | yes      | The Esendex account                                    |
| login        | String | yes      | The login to access the API                            |
| password     | String | yes      | The password to access the API                         |

```yaml
- provider: esendex
  id: esendex
  account: EX123456
  login: login
  password: pwd
```

### AllMySms

Provider identifier: `allMySms`

Use the [AllMySms](https://www.allmysms.com/) API to send SMS. You need an account to use this provider.
The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| login        | String | yes      | The login to access the API                            |
| apiKey       | String | yes      | The key to access the API                              |

```yaml
- provider: allMySms
  id: allMySms
  login: fabar
  apiKey: 123456789
```

### Mailjet SMS

Provider identifier: `mailjetSms`

Use the [mailjet](https://www.mailjet.com/) API to send SMS. You need an account to use this provider.
The following configuration is needed:

| Key          | Type   | Required | Description                                            |
|--------------|--------|----------|--------------------------------------------------------|
| token        | String | yes      | The token to access the API                            |

```yaml
- provider: mailjetSms
  id: mailjetSms
  token: 123456789
```

## Add other providers

As you can see, the supported providers are limited. To add other providers you can:

* create an [issue in Gitlab](https://gitlab.com/sirius6/notification-service/-/issues) and let me develop it. The community will enjoy it!
* fork the project. The community will not enjoy it!
* join the project to help me!
