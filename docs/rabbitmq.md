# RabbitMQ

[[_TOC_]]

RabbitMQ is used to send the notifications asynchronously. All the following exchange and queue name start with "sirius."
This prefix is the default one, but it can be changed via the [configuration file](configuration.md#notification-configuration).


## Submission

The application uses the main RabbitMQ "direct" exchange named `surius.notification`.

The following queue are bind to this exchange:

| Queue name                    | Routing key                  | Description                                   |
|-------------------------------|------------------------------|-----------------------------------------------|
| sirius.notification.email     | notification.email           | Queue receiving emails messages               |
| sirius.notification.sms       | notification.sms             | Queue receiving SMS messages                  |
| sirius.notification.webhook   | notification.webhook         | Queue receiving webhook messages              |
| sirius.notification.websocket | notification.websocket.topic | Queue receiving websocket topic messages      |

Each queue get multiple consumers, but you can also deploy multiple notification services.


## Dead letter

For robustness reason, a "dead letter" fanout exchange is also present with name `sirius.notification.dlx`.
A queue named `sirius.notification.dlq` is bind to this exchange.

When an error occurs in the main exchange, the message is sent to the "dead letter" exchange. If it does not reach
the retries limit, the message is requeue to the main exchange.


## Callback

When a notification is sent successfully or if the sending fails, a specific message is sent to the "topic" exchange
named `sirius.notification.callback`. There is two routing keys depending on the success or failure, and another one for the tracking (see below).

There is no queue bind by this exchange. To get the callback, you have to bind your own queues.


### Notification success

The routing key is `notification.{notificationType}.success`

Here are the variables in the routing key:

| Variable         | Description                                                |
|------------------|------------------------------------------------------------|
| notificationType | Can be `EMAIL`, `SMS`, `WEBSOCKET` or `WEBHOOK`            |

The message sent in this routing key looks like this:

```json
{
  "notification": {
    "id": "bcda8670-40a3-11eb-ab09-4f3427bff8cc",
    "type": "EMAIL"
  },
  "provider": {
    "id": "gmail",
    "type": "smtp"
  },
  "tryIndex": 0,
  "counter": 1,
  "metadata": {
    "mykey": "myvalue"
  }
}
```

| Key              | Description                                                                                                      |
|------------------|------------------------------------------------------------------------------------------------------------------|
| notification.id  | The notification unique identifier (set during notification creation)                                            | 
| notification.type| Can be `EMAIL`, `SMS`, `WEBSOCKET` or `WEBHOOK`                                                                  |
| provider.id      | The provider identifier as defined in the [descriptor](providers.md#descriptor)                                  |
| provider.type    | The provider type as defined in the [descriptor](providers.md#descriptor) (ie. "smtp" or "mandrill" )            |
| tryIndex         | The try index (starts at 0). Incremented when sending fails and retried                                          |
| counter          | The units of the notification. Only useful for SMS because depending on the message length, many SMS can be sent |
| metadata         | Key / values set during notification creation (useful to identify a specific notification                        |


### Notification failure

The routing key is `notification.{notificationType}.error`

Here are the variables in the routing key:

| Variable         | Description                                                |
|------------------|------------------------------------------------------------|
| notificationType | Can be `EMAIL`, `SMS`, `WEBSOCKET` or `WEBHOOK`            |

The message sent in this routing key looks like this:

```json
{
  "notification": {
    "id": "bcda8670-40a3-11eb-ab09-4f3427bff8cc",
    "type": "EMAIL"
  },
  "tries": 5,
  "metadata": {
    "mykey": "myvalue"
  }
}
```

| Key              | Description                                                                               |
|------------------|-------------------------------------------------------------------------------------------|
| notification.id  | The notification unique identifier (set during notification creation)                     | 
| notification.type| Can be `EMAIL`, `SMS`, `WEBSOCKET` or `WEBHOOK`                                           |
| tries            | The tries count before failing the notification                                           |
| metadata         | Key / values set during notification creation (useful to identify a specific notification |


### Notification tracking

**For now, only email notification supports tracking.**

The routing key is `notification.{notificationType}.tracking.{trackingEventType}`

| Variable         | Description                                                |
|------------------|------------------------------------------------------------|
| notificationType | Can be `email`, `sms`, `websocket` or `webhook`            |
| trackingType     | The tracking event type in lower case (see below)          |

The following tracking event type are available:

| Type     | Description                                                                                              |
|----------|----------------------------------------------------------------------------------------------------------|
| SENT     | The message has been sent (you should avoid listening this event because it can flood your application!) |
| OPEN     | The message has been opened by the recipient                                                             |
| CLICK    | A link has been clicked in the message by the recipient                                                  |
| BOUNCE   | The message has not been delivered (invalid address, blacklist, etc.)                                    |
| BLOCKED  | The message has been blocked (in most case when the sender email is not granted)                         |
| SPAM     | The message has been received in spam box                                                                |
| UNSUB    | The recipient is unsubscribed from a mail list                                                           |
| DEFERRED | The message has not been delivered yet (delayed)                                                         |

The message sent in this routing key looks like this:

```json
{
  "notification": {
    "id": "bcda8670-40a3-11eb-ab09-4f3427bff8cc",
    "type": "EMAIL"
  },
  "provider": {
    "id": "gmail",
    "type": "smtp"
  },
  "type": "OPEN",
  "timestamp": "2000-01-23T01:23:45Z",
  "ipAddress": "192.168.0.1",
  "userAgent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
  "email": "sirius@test.com",
  "clickedUrl": "https://gitlab.com",
  "metadata": {
    "mykey": "myvalue"
  }
}
```

| Key              | Description                                                                                |
|------------------|--------------------------------------------------------------------------------------------|
| notification.id  | The notification unique identifier (set during notification creation)                      | 
| notification.type| Can be `EMAIL`, `SMS`, `WEBSOCKET` or `WEBHOOK`                                            |
| provider.id      | The provider identifier as defined in the [descriptor](providers.md#descriptor)            |
| provider.type    | The provider type as defined in the [descriptor](providers.md#descriptor) (ie. "smtp" or "mandrill" ) |
| metadata         | Key / values set during notification creation (useful to identify a specific notification  |
| type             | The tracking event type (see above)                                                        |
| timestamp        | The date when occurred the event (ISO8601)                                                 |
| ipAddress        | The user IP address when the event has been triggered (not available on all events)        |
| userAgent        | The browser user agent when click on a link in the message                                 |
| email            | The email of the recipient who triggered the event (a message can have many recipients)    |
| clickedUrl       | The link clicked in the email                                                              |