# Install

## Standalone

The application can be launched by using Java 11.

You have to follow these steps:

1. download the last application version on [repository](https://gitlab.com/sirius6/notification-service/-/packages)
2. prepare the [providers.yml](providers.md#descriptor) and save it in the same directory of the JAR
3. prepare the `application.yml` [configuration](configuration.md) and save it in the same directory of the JAR
4. launch the application with `java -jar notification-service-{version}.jar -Xmx256m`


## Docker

The project releases a JAR file, and a Docker image. You can use it in a "docker run", docker-compose or "docker swarm".

The image name is `fabar/sirius-notification`.   
It is hosted on Docker Hub: [https://hub.docker.com/r/fabar/sirius-notification](https://hub.docker.com/r/fabar/sirius-notification)

In all cases you must mount these volumes:

* `/app/providers.yml` to set the [providers.yml](providers.md#descriptor)
* `/app/application.yml` to set the [application.yml](configuration.md).

### Docker run

You can execute the following command to start the server:

```
docker run \
   -p 8080:8080 \
   -v ./docker/providers.yml:/app/providers.yml \
   -v ./docker/application.yml:/app/application.yml \
   fabar/sirius-notification
```

The RabbitMQ must be already running and be configured in the [application.yml](configuration.md).

### Docker compose

You can use the last version in the following docker-compose file:

```yaml
version: '3.7'

services:

  rabbitmq:
    image: rabbitmq:3.8-management
    container_name: notification-mq
    ports:
      - "15672:15672"
    volumes:
      - ${HOME}/.sirius/rabbitmq:/var/lib/rabbitmq
    healthcheck:
      test: rabbitmq-diagnostics -q ping

  maildev:
    image: maildev/maildev
    container_name: notification-maildev
    ports:
      - "1080:80"

  server:
    image: fabar/sirius-notification
    container_name: notification-server
    depends_on:
      - rabbitmq
    ports:
      - "8080:8080"
    volumes:
      - ./docker/providers.yml:/app/providers.yml
      - ./docker/application.yml:/app/application.yml
```

The `maildev` is a basic SMTP server. It displays sent emails. It is useful for local tests.

You can use the following `application.yml` when using compose:

```yaml
spring:
  rabbitmq:
    host: rabbitmq

notification:
  descriptorPath: file:providers.yml
  stomp:
    host: rabbitmq
```